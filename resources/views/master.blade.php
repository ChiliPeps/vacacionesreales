<!doctype html>
<html lang="es">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<meta name="csrf-token" id="csrf-token" content="{{ csrf_token() }}">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="#descripcion Vacaciones reales">
	<meta name="keywords" content="en edicion...">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	{{-- Titulo --}}
	<title> @yield('title') Vacaciones Reales ©.</title>

	<!-- Metas -->
	@hasSection('metas')
		@yield('metas')
	@else
        <meta property="fb:app_id" content=""/>
		<meta property="og:title" content="Vacaciones Reales">
		<meta property="og:url" content="https://vacacionesreales.com"/>
		<meta property="og:description" content="#descripcion de vacacionesreales.">
		<meta property="og:image" content=""/>
	@endif

	{!! Html::favicon('favicon.ico') !!}
	@include('inc/header_common')

</head>
<body class="body">
	@include('inc/header')

	<!-- Content -->
	@yield('content')
	<!-- Content -->

	@include('inc/footer')
	@include('inc/footer_common')
</body>
</html>