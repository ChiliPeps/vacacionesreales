@extends('master')

@section('content')
    <div id="contentapp" v-cloak>
        @include('content.blog.entrada')
    </div>
@stop

@push('scripts')
<script>
  $(document).ready(function() {
      var heightSlider = $('.navbar').outerHeight();
      var pixels = heightSlider + 50;
      $('.principal').css({ marginTop : pixels  + 'px' });
  });
</script>


@endpush


@push('css')
<style>
.principal {
	position: relative !important;
	text-align: center !important;
	color: white;
}

/* Bottom left text */
.bottom-left {
  position: absolute;
  bottom: 220px;
  left: 16px;
  font-size:60px;
}

	/* Centered text */
.centered {
 	position: absolute;
  	top: 50%;
  	left: 50%;
  	transform: translate(-50%, -50%);
}
.black{
	color:black;
}
</style>
@endpush