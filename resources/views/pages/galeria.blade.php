@extends('master')

@section('content')
    <div id="contentapp" v-cloak>
        @include('content.galeria.galeria')
    </div>
@stop



@push('scripts')
	{!! Html::script('js/bootstrapstudio/Filterable-Gallery-with-Lightbox.js') !!}
@endpush