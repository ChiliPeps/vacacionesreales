@extends('master')

@section('content')
    <div id="contentapp" v-cloak>
        @include('content.destino.destino')
    </div>
@stop

@push('scripts')
	{!! Html::script('js/bootstrapstudio/Filterable-Gallery-with-Lightbox.js') !!}
@endpush