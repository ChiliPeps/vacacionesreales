<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="text-uppercase section-heading">Contacto</h2>
                <h3 class="section-subheading text-muted"></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <form id="contactForm" name="contactForm" novalidate="novalidate">
                    <div class="form-row">
                        <div class="col col-md-6">
                            <div class="form-group"><input type="text" required class="form-control" id="name" placeholder="Nombre *" /><small class="form-text text-danger flex-grow-1 help-block lead"></small></div>
                            <div class="form-group"><input type="email" required class="form-control" id="email" placeholder="Email *" /><small class="form-text text-danger help-block lead"></small></div>
                            <div class="form-group"><input type="tel" required class="form-control" placeholder="Telefono *" /><small class="form-text text-danger help-block lead"></small></div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group"><textarea required class="form-control" id="message" placeholder="Escribenos *"></textarea><small class="form-text text-danger help-block lead"></small></div>
                        </div>
                        <div class="col">
                            <div class="clearfix"></div>
                        </div>
                        <div class="col-lg-12 text-center">
                            <div id="success"></div><button class="btn btn-info btn-xl text-uppercase" type="submit"                            id="sendMessageButton">Enviar</button>
                            </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>