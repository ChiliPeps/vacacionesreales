<h2 class="display-3 text-center text-dark" style="margin: -13px;padding: -21px;margin-top: 187px; margin-bottom:70px;">Hoteles</h2><section class="showcase">
    <div class="container-fluid p-0">
        <div class="row no-gutters">
            <div class="col-lg-6 order-lg-2 text-white showcase-img" style="background-image:url('img/Alberca.jpg');"><span></span></div>
            <div class="col-lg-6 my-auto order-lg-1 showcase-text">
                <h2>RIU Los Cabos</h2>
                <p class="lead mb-0">En el extremo sur de la península de Baja California se encuentra Los Cabos, uno de los destinos más bellos y exclusivos de México. Esta zona, situada entre San José del Cabo y Cabo San Lucas, sorprende por la multitud de atractivos que ofrece: playas excepcionales, bellos paisajes, deportes acuáticos y una variada oferta de ocio tanto diurno como nocturno.</p>
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col-lg-6 text-white showcase-img" style="background-image:url('img/ChiquiClub.jpg');"><span></span></div>
            <div class="col-lg-6 my-auto order-lg-1 showcase-text">
                <h2>Royal Solaris </h2>
                <p class="lead mb-0">Royal Solaris los Cabos resort and SPA, se encuentra entre los mejores hoteles en Los Cabos México que ofrecen vacaciones todo incluido para familias. Está localizado a 20 minutos del aeropuerto internacional de San José del Cabo y a 25 minutos de Cabo San Lucas. El hotel tiene instalaciones para toda la familia, ofrece excelente servicio, actividades durante el día en la piscina, espectáculos nocturnos y y un mini parque acuático infantil para los más pequeños. El hotel tiene con 390 habitaciones frente al mar de Cortés y vista a la ciudad.</p>
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col-lg-6 order-lg-2 text-white showcase-img" style="background-image:url('img/frutas.jpg');"><span></span></div>
            <div class="col-lg-6 my-auto order-lg-1 showcase-text">
                <h2>HolidayInn Resorts</h2>
                <p class="lead mb-0">In id tortor libero. Donec tincidunt dolor eget velit congue laoreet. Suspendisse lobortis vel justo vitae tincidunt. Phasellus ac neque elementum, gravida nisi a, ullamcorper dolor. Aenean nisl nibh.</p>
            </div>
        </div>
    </div>
</section>