<div class="photo-gallery"></div>

<section id="filtr-gallery" style="padding-top: 250px;">
    <div class="container">
        <h1 class="text-center">Galeria de imagenes</h1>
        <div class="filtr-controls"><span class="active" data-filter="all">TODOS</span><span data-filter="1">HOTEL 1</span><span data-filter="2">HOTEL 2</span><span data-filter="2">HOTEL 3</span><span data-filter="3">HOTEL 4</span></div>
        <div class="row filtr-container">
            <div class="col-6 col-sm-4 col-md-3 filtr-item" data-category="1, 3"><a href="https://source.unsplash.com/qa4y7mBCa9U/900x1200.jpg"><img class="img-fluid" src="https://source.unsplash.com/qa4y7mBCa9U/600x600.jpg" alt="image" data-caption="<strong>Image description</strong><br><em>Lorem ipsum</em>"></a></div>
            <div
                    class="col-6 col-sm-4 col-md-3 filtr-item" data-category="2, 3"><a href="https://source.unsplash.com/r53rNKb_7s8/1200x900.jpg"><img class="img-fluid" src="https://source.unsplash.com/r53rNKb_7s8/600x450.jpg" alt="image"></a></div>
            <div class="col-6 col-sm-4 col-md-3 filtr-item" data-category="1"><a href="https://source.unsplash.com/eWFdaPRFjwE/1200x1200.jpg"><img class="img-fluid" src="https://source.unsplash.com/eWFdaPRFjwE/600x600.jpg" alt="image"></a></div>
            <div class="col-6 col-sm-4 col-md-3 filtr-item" data-category="1"><a href="https://source.unsplash.com/gF7hhMIC3vo/1200x900.jpg"><img class="img-fluid" src="https://source.unsplash.com/gF7hhMIC3vo/600x450.jpg" alt="image"></a></div>
            <div class="col-6 col-sm-4 col-md-3 filtr-item" data-category="2"><a href="https://source.unsplash.com/F3ePNdQb_Lg/1200x900.jpg"><img class="img-fluid" src="https://source.unsplash.com/F3ePNdQb_Lg/600x450.jpg" alt="image"></a></div>
            <div class="col-6 col-sm-4 col-md-3 filtr-item" data-category="2"><a href="https://source.unsplash.com/xFunHeSh3kU/1200x1200.jpg"><img class="img-fluid" src="https://source.unsplash.com/xFunHeSh3kU/600x600.jpg" alt="image"></a></div>
            <div class="col-6 col-sm-4 col-md-3 filtr-item" data-category="1, 3"><a href="https://source.unsplash.com/jqvxcyVx2YE/900x1600.jpg"><img class="img-fluid" src="https://source.unsplash.com/jqvxcyVx2YE/600x600.jpg" alt="image"></a></div>
            <div class="col-6 col-sm-4 col-md-3 filtr-item" data-category="2"><a href="https://source.unsplash.com/cFplR9ZGnAk/1200x1200.jpg"><img class="img-fluid" src="https://source.unsplash.com/cFplR9ZGnAk/600x600.jpg" alt="image"></a></div>
            <div class="col-6 col-sm-4 col-md-3 filtr-item" data-category="2, 3"><a href="https://source.unsplash.com/duo-xV0TU7s/1200x900.jpg"><img class="img-fluid" src="https://source.unsplash.com/duo-xV0TU7s/600x450.jpg" alt="image"></a></div>
            <div class="col-6 col-sm-4 col-md-3 filtr-item" data-category="1"><a href="https://source.unsplash.com/T7K4aEPoGGk/1200x900.jpg"><img class="img-fluid" src="https://source.unsplash.com/T7K4aEPoGGk/600x450.jpg" alt="image"></a></div>
        </div>
    </div>
</section>
