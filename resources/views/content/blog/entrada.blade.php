
<!-- Page Content -->
<div class="container principal">

  <img class="img-fluid" src="http://placehold.it/1920x1280" alt="">
  <div class="row black">
    <div class="col-md-6 text-left"> Por: <strong>Cesar Villavicencio</strong></div>
    <div class="col-md-6 text-right">Miercoles 17 de Junio, 2020</div>
  </div>
  
  <div class="col-lg-12 mt-30">
    <h1> Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h1>
  </div>   

</div>

  <div class="container">

    <div class="row">

      <!-- Post Content Column -->
      <div class="col-lg-8">
        <hr>

        <div class="social-icons">
          <a href="#"><i class="icon ion-social-whatsapp"></i></a>
          <a href="#"><i class="icon ion-social-facebook"></i></a>
          <a href="#"><i class="icon ion-social-twitter"></i></a>
          <a href="#"><i class="icon ion-social-instagram"></i></a>
        </div>

        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec luctus turpis nibh, in blandit velit congue ut. Morbi id orci eget augue maximus pharetra in ac tortor. Praesent nec consequat est, id auctor ex. In sit amet tempus velit. Morbi dui nibh, faucibus vel purus quis, convallis luctus tellus. Vestibulum pretium consectetur orci, non aliquet dolor consectetur nec. Aenean ac laoreet neque. Integer sed felis mollis enim dignissim vulputate eget at massa. Integer egestas ultricies turpis ut vestibulum.
        </p>

        <p>
          Nunc condimentum, turpis a accumsan pretium, ligula purus placerat nunc, at maximus nibh nisl quis libero. Nunc commodo libero vitae feugiat semper. Aliquam ligula ipsum, rutrum eu vehicula at, fringilla elementum tortor. Suspendisse feugiat urna metus, quis porta purus cursus sollicitudin. Nam nec feugiat neque. Donec eu eros consequat, hendrerit quam efficitur, eleifend ex. Sed sed felis tincidunt, dictum metus a, tristique ex. Nulla eu consequat orci, non sagittis justo. Nunc a tincidunt lacus, non sollicitudin lacus. Sed sed varius est. Etiam vitae diam efficitur mauris molestie lacinia. Nam nisl mauris, faucibus ac aliquet vel, iaculis quis elit. Quisque sit amet ullamcorper tortor, a porttitor ex.
        </p>

        <p>
         Cras quis ex eu mauris bibendum volutpat ut eu metus. Phasellus id leo nisi. Praesent in consectetur augue. Donec in felis nec mauris ultricies bibendum. Nam ex lacus, lobortis at lorem quis, elementum euismod elit. Sed aliquet hendrerit justo vel tincidunt. Nullam varius mi sem, sit amet sagittis velit luctus non. Phasellus pretium lectus vehicula, ullamcorper ante ac, tincidunt nisi.
        </p>

        <p>
          Morbi orci sapien, molestie at arcu sagittis, maximus bibendum augue. Cras vitae neque non nisi eleifend dapibus. Nam eu bibendum dolor. Maecenas faucibus id diam non mollis. Morbi fringilla nulla vel massa interdum, at egestas odio eleifend. Suspendisse vel blandit diam. Curabitur imperdiet orci sit amet neque sodales, id bibendum sem elementum. Maecenas purus tortor, molestie vitae sapien a, rutrum finibus velit. Aliquam tincidunt urna sit amet vulputate fringilla. Proin posuere ornare ante, vitae tincidunt ante. Aenean dapibus erat mauris, vitae faucibus tortor suscipit eget. Suspendisse vitae neque maximus, eleifend elit a, pellentesque quam. Aliquam arcu eros, faucibus id scelerisque quis, cursus ut sapien.
        </p>

        <p>
          Praesent vulputate ante ex, eget bibendum ligula placerat eget. Ut cursus velit et lectus tincidunt volutpat. Etiam a pretium leo. Nulla varius semper urna, sit amet cursus ex varius at. Nunc luctus felis dolor, in placerat felis porta eget. Maecenas vestibulum leo efficitur risus condimentum elementum. Aenean congue nulla non nibh tristique, ac tempus nibh sodales. In tempor feugiat elit, in vestibulum odio sagittis quis. Aenean malesuada enim nisi, eu interdum lorem porttitor a. Duis facilisis ante nisi, eu viverra ex sodales sed. Vivamus sodales et est id hendrerit. In molestie sapien in mollis ullamcorper.
        </p>
        <!-- Post Content -->
       {{--  <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut, error quam sapiente nemo saepe quibusdam sit excepturi nam quia corporis eligendi eos magni recusandae laborum minus inventore?</p>

        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut, tenetur natus doloremque laborum quos iste ipsum rerum obcaecati impedit odit illo dolorum ab tempora nihil dicta earum fugiat. Temporibus, voluptatibus.</p>

        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, doloribus, dolorem iusto blanditiis unde eius illum consequuntur neque dicta incidunt ullam ea hic porro optio ratione repellat perspiciatis. Enim, iure!</p>

        <blockquote class="blockquote">
          <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
          <footer class="blockquote-footer">Someone famous in
            <cite title="Source Title">Source Title</cite>
          </footer>
        </blockquote>

        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>

        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p> --}}

        <hr>
        <div class="social-icons">
          <a href="#"><i class="icon ion-social-whatsapp"></i></a>
          <a href="#"><i class="icon ion-social-facebook"></i></a>
          <a href="#"><i class="icon ion-social-twitter"></i></a>
          <a href="#"><i class="icon ion-social-instagram"></i></a>
        </div>
        <hr>

        <!-- Comments Form -->
        <div class="card my-4">
          <h5 class="card-header">Leave a Comment:</h5>
          <div class="card-body">
            <form>
              <div class="form-group">
                <textarea class="form-control" rows="3"></textarea>
              </div>
              <button type="submit" class="btn btn-primary">Submit</button>
            </form>
          </div>
        </div>

        <!-- Single Comment -->
        <div class="media mb-4">
          <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
          <div class="media-body">
            <h5 class="mt-0">Commenter Name</h5>
            Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
          </div>
        </div>

        <!-- Comment with nested comments -->
        <div class="media mb-4">
          <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
          <div class="media-body">
            <h5 class="mt-0">Commenter Name</h5>
            Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.

            <div class="media mt-4">
              <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
              <div class="media-body">
                <h5 class="mt-0">Commenter Name</h5>
                Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
              </div>
            </div>

            <div class="media mt-4">
              <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
              <div class="media-body">
                <h5 class="mt-0">Commenter Name</h5>
                Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
              </div>
            </div>

          </div>
        </div>

      </div>

      <!-- Sidebar Widgets Column -->
      <div class="col-md-4">

{{--         <!-- Categories Widget -->
        <div class="card my-4">
          <h5 class="card-header">Categories</h5>
          <div class="card-body">
            <div class="row">
              <div class="col-lg-6">
                <ul class="list-unstyled mb-0">
                  <li>
                    <a href="#">Web Design</a>
                  </li>
                  <li>
                    <a href="#">HTML</a>
                  </li>
                  <li>
                    <a href="#">Freebies</a>
                  </li>
                </ul>
              </div>
              <div class="col-lg-6">
                <ul class="list-unstyled mb-0">
                  <li>
                    <a href="#">JavaScript</a>
                  </li>
                  <li>
                    <a href="#">CSS</a>
                  </li>
                  <li>
                    <a href="#">Tutorials</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div> --}}

        <!-- Side Widget -->
        <div class="card my-4">
          <h5 class="card-header">Ultimas entradas</h5>
          {{-- <div class="card-body"> --}}
           
                <img class="card-img-top" src="http://placehold.it/1920x1080" alt="Card image cap">
                <div class="card-body">
                  <h2 class="card-title">Post Title</h2>
                  <p class="card-text">Lorem ipsum dolor sit amet</p>
                  <a href="{{route('entrada')}}" class="btn btn-primary">Leer</a>
                </div>
                <div class="card-footer text-muted">
                  Posted on January 1, 2017 by
                  <a href="#">Start Bootstrap</a>
                </div>
          {{-- </div> --}}
        </div>

      </div>

    </div>
    <!-- /.row -->

  </div>