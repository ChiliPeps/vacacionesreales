<div class="simple-slider">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <div class="swiper-slide" style="background-image: url('img/1.jpeg');"></div>
                <div class="swiper-slide" style="background-image: url('img/2.jpeg');"></div>
                <div class="swiper-slide" style="background-image: url('img/3.jpeg');"></div>
            </div>
            <div class="swiper-pagination"></div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
        </div>
    </div>

    <div class="container">
    </div>

    <div class="social-icons"><a href="#"><i class="icon ion-social-whatsapp"></i></a><a href="#"><i class="icon ion-social-facebook"></i></a><a href="#"><i class="icon ion-social-twitter"></i></a><a href="#"><i class="icon ion-social-instagram"></i></a></div>

    <div class="container">
        <div class="card-group" style="padding-bottom: 50px;">
            <div class="card" style="padding-right: 10px;padding-left: 10px;border: 0;"><img class="card-img-top w-100 d-block" src="img/paralax.jpg" style="padding-right: 0px;">
                <div class="card-body text-center border rounded-0">
                    <div style="padding-bottom: 15px;"><i class="fab fa-whatsapp" style="padding-right: 5px;padding-left: 5px;"></i><i class="fa fa-facebook-f" style="padding-right: 5px;padding-left: 5px;"></i><i class="fab fa-twitter" style="padding-right: 5px;padding-left: 5px;"></i>
                        <i
                            class="fab fa-instagram" style="padding-right: 5px;padding-left: 5px;"></i>
                    </div>
                    <h4 class="text-center card-title">$ 2,975.00</h4>

                    <ul class="list-group" style="border: 0 !imnportant;">

                        <li class="list-group-item" style="border: 0;">
                          <span>Todo Incluido , Shows, Activiades&nbsp;🍹🍸&nbsp;🍽</span>
                        </li>

                        <li class="list-group-item" style="border: 0;">
                          <span>24 Hrs DE Alimentos y Bebidas</span>
                        </li>

                        <li class="list-group-item" style="border: 0;">
                          <span>Hotel&nbsp;⭐⭐⭐⭐⭐&nbsp;Riu Santa Fe</span>
                        </li>

                        <li class="list-group-item" style="border: 0;">
                          <span>Hotel a pie de PLAYA ⛱</span>
                        </li>

                         <li class="list-group-item" style="border: 0;">
                          <span>Bebidas 🍹🍸</span>
                        </li>

                         <li class="list-group-item" style="border: 0;">
                          <span>Alimentos 🍽</span>
                        </li>

                         <li class="list-group-item" style="border: 0;">
                          <span>Impuestos 💸</span>
                        </li>

                        <li class="list-group-item" style="border: 0;">
                          <span>Niños menores de 4 años gratis </span>
                        </li>

                        

                        <li class="list-group-item" style="border: 0;">
                          <span>Paquete válido hasta agotar existencias</span>
                        </li>

                    </ul><button class="btn btn-primary btn-sm bounce animated" type="button" style="background-color: rgb(0,221,251);filter: sepia(0%);margin-top: 15px;">Reservar</button></div>
            </div>

            <div class="card" style="padding-right: 10px;padding-left: 10px;border: 0;">
              <img class="card-img-top w-100 d-block" src="img/frutas.jpg" style="padding-right: 0px;">
                <div class="card-body text-center border rounded-0"><i class="fa fa-star"></i>
                    <h4 class="text-center card-title">$ 2,975.00</h4>
                    <ul class="list-group" style="border: 0 !imnportant;">
                        <li class="list-group-item" style="border: 0;">
                          <span>Todo Incluido , Shows, Activiades&nbsp;🍹🍸&nbsp;🍽</span>
                        </li>

                        <li class="list-group-item" style="border: 0;">
                          <span>Hospedaje 1 noches y 2 días&nbsp;🏨</span>
                        </li>

                        <li class="list-group-item" style="border: 0;">
                          <span>Hotel&nbsp;⭐⭐⭐⭐&nbsp;Holiday inn resort San Jose Del Cabo</span>
                        </li>

                        <li class="list-group-item" style="border: 0;">
                          <span>se requiere pago total para poder garantizar esa tarifa</span>
                        </li>

                        <li class="list-group-item" style="border: 0;">
                          <span>Paquete válido hasta agotar existencias</span>
                        </li>

                    </ul>
                    <button class="btn btn-primary btn-sm bounce animated" type="button" style="background-color: rgb(0,221,251);filter: sepia(0%);margin-top: 15px;">Reservar</button>
                  </div>
            </div>

            <div class="card" style="padding-right: 10px;padding-left: 10px;border: 0;">
              <img class="card-img-top w-100 d-block" src="img/Fachada.jpg" style="padding-right: 0px;">
                <div class="card-body text-center border rounded-0"><i class="fa fa-star"></i>
                    <h4 class="text-center card-title">$ 2,975.00</h4>
                    <ul class="list-group" style="border: 0 !imnportant;">
                        <li class="list-group-item" style="border: 0;">
                          <span>Todo Incluido , Shows, Activiades&nbsp;🍹🍸&nbsp;🍽</span>
                        </li>

                        <li class="list-group-item" style="border: 0;">
                          <span>Hospedaje 1 noches y 2 días&nbsp;🏨</span>
                        </li>

                        <li class="list-group-item" style="border: 0;">
                          <span>Hotel&nbsp;⭐⭐⭐⭐&nbsp;Holiday inn resort San Jose Del Cabo</span>
                        </li>

                        <li class="list-group-item" style="border: 0;">
                          <span>se requiere pago total para poder garantizar esa tarifa</span>
                        </li>

                        <li class="list-group-item" style="border: 0;">
                          <span>Paquete válido hasta agotar existencias</span>
                        </li>

                    </ul>
                    <button class="btn btn-primary btn-sm bounce animated" type="button" style="background-color: rgb(0,221,251);filter: sepia(0%);margin-top: 15px;">Reservar</button>
                  </div>
            </div>
        </div>
    </div>