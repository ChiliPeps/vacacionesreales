<div class="jumbotron hero-photography" style="background-image:url('../img/riu/riuHero.jpg');">
    <h1 class="hero-title">Riu Santa Fe, Los Cabos, B.C.S. MÉXICO</h1>
        {{-- <p class="hero-subtitle">Nullam id dolor id nibh ultricies vehicula ut id elit. Cras justo odio, dapibus ac facilisis in, egestas eget quam.</p> --}}
        {{-- <p><a class="btn btn-primary btn-lg hero-button" role="button" href="#">Learn more</a></p> --}}
</div>

 <div class="social-icons">
  <a href="#"><i class="icon ion-social-whatsapp"></i></a>
  <a href="#"><i class="icon ion-social-facebook"></i></a>
  <a href="#"><i class="icon ion-social-twitter"></i></a>
  <a href="#"><i class="icon ion-social-instagram"></i></a>
</div>
<hr>
<div class="container">
    <div class="intro">
        <h2 class="text-center">Riu Santa Fe, Los Cabos, B.C.S. México</h2>
        <p class="text-center"> </p>
    </div>
    <div class="row mt-5">
        <div class="col-md-8" style="background-color: white;">
            <h2>Descripcion</h2>
            <div>
                <p>
                    Salidas: Sábados, Lunes y Jueves
                </p>

                <p>
                <h3 style="color: blue;"> Día 1: Chihuahua </h3>

                Llegada por su cuenta a la ciudad de Chihuahua. Sus documentos de viaje (cupones de hoteles, boletos de tren e itinerario) estarán en la recepción de su hotel; favor de solicitarlos a su registro.

                Este día a las 3:00 pm paseo guiado por los principales puntos de interés de la ciudad visitando el Museo de Pancho Villa (cerrado los lunes), Palacio de Gobierno y sus murales descriptivos de la historia del estado, Catedral, Acueducto Colonial y la zona residencial. Cena ligera tipo box incluida en el hotel.

                Hoy: Cena incluida

                Hospedaje: Hotel Casa Grande o similar / sujeto a disponibilidad

                *Favor de considerar los siguientes horarios de hotel: Check-in a las 3:00 pm y Check-out a la 12:00 pm

                *El estado de Chihuahua y Sinaloa manejan horario “zona montaña”, favor de ajustar su reloj una hora más temprano que el resto del país.
                </p>

                <p>
                <h3 style="color: blue;"> Día 2: Chihuahua-Creel </h3>

                Esta mañana a las 6:00 am serán trasladados por personal de Viajes Dorados a la estación de autobuses de Autotransportes Turísticos Noroeste para abordar su autobús a las 6:45 am con destino a Creel. Llegada a Creel a las 11:00 pm aproximadamente donde serán recibidos y trasladados a su hotel por personal del mismo. Tarde libre para disfrutar de este pintoresco Pueblo Mágico. Esta tarde le sugerimos tomar un paseo (opcional-no incluido) por los alrededores, favor de solicitar informes en la recepción de su hotel.

                Hospedaje: Hotel Real de Creel o similar / sujeto a disponibilidad
                </p>

                <p>
                <h3 style="color: blue;"> Día 3: Creel-Divisadero </h3>
                

                Desayuno incluido en el hotel. Esta mañana a las 10:30 am traslado por personal del hotel a la estación de autobuses de Autotransportes Turísticos Noroeste para abordar a las 11:00 am su autobús con destino a Divisadero. Llegada a las 12:00 pm aproximadamente favor de dirigirse directamente a su hotel que se encuentra a escasos pasos de la estación del tren; si requiere de ayuda con su equipaje no dude en solicitarlo en la recepción. Esta tarde sugerimos; visita al Parque Aventura Barrancas del Cobre que se encuentra en el área y donde podrá visitar los distintos miradores, mirador piedra volada, retar su espíritu aventurero en el puente colgante que cruza un pequeño cañón, paseo en el Teleférico o Tirolesas (opcional-no incluidos), o simplemente comprar bonita artesanía Tarahumara.

                Hoy: Desayuno incluido

                Hospedaje: Hotel Divisadero o similar / sujeto a disponibilidad

                Esta tarde disfrute de la maravillosa puesta del sol que la Barranca del Cobre nos regala.
                </p>
                <p>
                <h3 style="color: blue;"> Día 4: Divisadero Barrancas-El Fuerte </h3>

                Todos Abordo!

                Desayuno incluido; luego favor de dirigirse directamente a la estación del tren para abordar el Tren Chepe Express clase turista a las 9:25 am con destino a la ciudad colonial de El Fuerte. Disfrute de la parte más interesante del recorrido en el tren al cruzar por varios puentes y túneles que engalanan las impresionantes vistas que la Sierra Madre ofrece.

                Llegada a El Fuerte “Pueblo Mágico” a las 3:05 pm donde serán recibidos por el Sr. Pedro Bernal (trasladista) y trasladados a su hotel. Esta tarde; camine por las calles empedradas, visite su iglesia colonial, el palacio municipal, el mercado, el museo local, también realice una relajante caminata por la orilla del río que cruza por un lado de su hotel o simplemente disfrute de las hermosas instalaciones de su hotel. También le recomendamos que deleite su paladar con ricos platillos de mar que le ofrecen los distintos restaurantes locales o el de su propio hotel; nuestra recomendación: Cauque o langostino de río!

                Hoy: Desayuno incluido

                Hospedaje: Hotel La Choza o similar / sujeto a disponibilidad

                CHEPE EXPRESS UP-GRADE a sección:

                Ejecutiva: $575.00 pesos por persona (incluye acceso al bar / no incluye alimentos)
                Primera: $1,860.00 pesos por persona (incluye comida y acceso preferencial a las instalaciones del tren como el bar, terraza y domo comedor)
                *Sujeto a disponibilidad / *Precio Publico

                </p>
                <p>
                <h3 style="color: blue;">
                    Día 5: Salida                        
                </h3>
                

                Traslado al aeropuerto de Los Mochis por el Sr. Pedro Bernal (trasladista), favor de considerar dos horas de traslado por carretera y dos horas previas de registro que las aerolíneas solicitan.

                RESERVACIONES: Contáctanos directamente o llena la forma de contacto y nos comunicaremos contigo!!!

                Lic. Analhy Rodríguez Viajes Dorados de Chihuahua: 45 años de experiencia nos avalan!! 01 800 696 1518 analhy@coppercanyon.com.mx

                Skype: analhy3@hotmail.com
                </p>
            </div>
        </div>
        <div class="col-md-4" style="background-color:#f1f1f1;">
            <h3>Detalles</h3>
            <hr>
            <div>
                valval
            </div>
            <hr>
            <div>
                <p>texto</p>
            </div>
        </div>    
    </div>
    
</div>

    <div class="features-clean">
        <div class="container">
            <div class="intro">
                <h2 class="text-center">Riu Santa Fe, Los Cabos, B.C.S. México</h2>
                <p class="text-center"> </p>
            </div>
            <div class="row features">
                <div class="col-sm-6 col-lg-4 item">
                    <h3 class="text-center name">Desayunos buffet</h3>
                    <p class="text-center">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est.</p>
                </div>

                <div class="col-sm-6 col-lg-4 item">
                    <h3 class="text-center name">Splash Water World</h3>
                    <p class="text-center">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est.</p>
                </div>

                <div class="col-sm-6 col-lg-4 item">
                    <h3 class="text-center name">Hotels all inclusive</h3>
                    <p class="text-center">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est.</p>
                </div>

                <div class="col-sm-6 col-lg-4 item">
                    <h3 class="text-center name">Renova Spa</h3>
                    <p class="text-center">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est.</p>
                </div>

                <div class="col-sm-6 col-lg-4 item">
                    <h3 class="text-center name">RIU POOL PARTY</h3>
                    <p class="text-center">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est.</p>
                </div>

                <div class="col-sm-6 col-lg-4 item">
                    <h3 class="text-center name">Restaurants</h3>
                    <p class="text-center">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est.</p>
                </div>
            </div>
        </div>
    </div>
    
    <div class="text-center">
        <iframe width="860" height="615" src="https://www.youtube.com/embed/AZoF8VlPpQY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>


    <section id="filtr-gallery" style="padding-top: 50px;">
    <div class="container">
        <h1 class="text-center">Galeria de imagenes</h1>

        <div class="filtr-controls">
            <span class="active" data-filter="1">Galeria</span>
        </div>

        <div class="row filtr-container">



            <div class="col-6 col-sm-4 col-md-3 filtr-item" data-category="1">
                <a href="../../public/img/riu/1.jpg">
                <img src="../../public/img/riu/1.jpg" class="img-fluid" alt="image" data-caption="<strong>Riu Los cabos </strong><br><em>B.C.S. México</em>">
                </a>
            </div>

             <div class="col-6 col-sm-4 col-md-3 filtr-item" data-category="1">
                <a href="../../public/img/riu/2.jpg">
                <img src="../../public/img/riu/2.jpg" class="img-fluid" alt="image" data-caption="<strong>Riu Los cabos </strong><br><em>B.C.S. México</em>">
                </a>
            </div>

             <div class="col-6 col-sm-4 col-md-3 filtr-item" data-category="1">
                <a href="../../public/img/riu/3.jpg">
                <img src="../../public/img/riu/3.jpg" class="img-fluid" alt="image" data-caption="<strong>Riu Los cabos </strong><br><em>B.C.S. México</em>">
                </a>
            </div>

             <div class="col-6 col-sm-4 col-md-3 filtr-item" data-category="1">
                <a href="../../public/img/riu/4.jpg">
                <img src="../../public/img/riu/4.jpg" class="img-fluid" alt="image" data-caption="<strong>Riu Los cabos </strong><br><em>B.C.S. México</em>">
                </a>
            </div>

             <div class="col-6 col-sm-4 col-md-3 filtr-item" data-category="1">
                <a href="../../public/img/riu/5.jpg">
                <img src="../../public/img/riu/5.jpg" class="img-fluid" alt="image" data-caption="<strong>Riu Los cabos </strong><br><em>B.C.S. México</em>">
                </a>
            </div>

             <div class="col-6 col-sm-4 col-md-3 filtr-item" data-category="1">
                <a href="../../public/img/riu/6.jpg">
                <img src="../../public/img/riu/6.jpg" class="img-fluid" alt="image" data-caption="<strong>Riu Los cabos </strong><br><em>B.C.S. México</em>">
                </a>
            </div>

             <div class="col-6 col-sm-4 col-md-3 filtr-item" data-category="1">
                <a href="../../public/img/riu/7.jpg">
                <img src="../../public/img/riu/7.jpg" class="img-fluid" alt="image" data-caption="<strong>Riu Los cabos </strong><br><em>B.C.S. México</em>">
                </a>
            </div>

             <div class="col-6 col-sm-4 col-md-3 filtr-item" data-category="1">
                <a href="../../public/img/riu/8.jpg">
                <img src="../../public/img/riu/8.jpg" class="img-fluid" alt="image" data-caption="<strong>Riu Los cabos </strong><br><em>B.C.S. México</em>">
                </a>
            </div>

             <div class="col-6 col-sm-4 col-md-3 filtr-item" data-category="1">
                <a href="../../public/img/riu/9.jpg">
                <img src="../../public/img/riu/9.jpg" class="img-fluid" alt="image" data-caption="<strong>Riu Los cabos </strong><br><em>B.C.S. México</em>">
                </a>
            </div>

           

        </div>
    </div>
</section>