    <div class="jumbotron hero-photography"style="margin-top: 0px; !important">
        
        {{-- <h2>Somos la mejor opcion para las vacaciones que tu familia se merece</h2> --}}
       
        {{-- <a href="{{route('contacto')}}">
            <button class="btn btn-info mb1 bg-blue">Contactanos</button>    
        </a> --}}
         
    </div>

    <div id="liquidBoxContainer"></div>

    <div class="article-list">
        <div class="container" style="max-width: 1240px !important;">
            <div class="intro">
                <h2 class="text-center">Destinos más visitados</h2>
                <p class="text-center">Vistia los mejores destinos de mexico</p>
            </div>
            <div class="row articles">
                <div class="col-md-3 item" >
                    <a href="{{route('destino')}}"><img class="img-fluid" src="img/riuPortada.jpg"></a>
                    <h3 class="name">Riu Los Cabos</h3>
                    <p class="description">El Hotel Riu Palace Cabo San Lucas está situado sobre una impresionante playa de arena blanca para que vivas unas vacaciones inolvidables. Este hotel en Los Cabos todo incluido 24 horas cuenta con una amplia oferta gastronómica, el mejor entretenimiento y con unas completas instalaciones para que disfrutes de un exclusivo hotel RIU en Los Cabos.</p>
                </div>

                <div class="col-md-3 item">
                   <a href="{{route('destino')}}"><img class="img-fluid" src="img/chepe/chepePortada.jpeg"></a>
                    <h3 class="name">El Chepe</h3>
                    <p class="description">"El Chepe" te llevará en un recorrido por dos de los atractivos naturales más imponentes de México. De la Sierra Tarahumara a las Barrancas del Cobre, internacionalmente conocidas por ser cuatro veces más grandes que el famoso Cañón del Colorado.</p>
                </div>

                <div class="col-md-3 item">
                    <a href="{{route('destino')}}"><img class="img-fluid" src="img/chiapas/portada.jpg"></a>
                    <h3 class="name">Chiapas</h3>
                    <p class="description">Chiapas, un destino para hacer turismo cultural Visitar Chiapas, estado con gran diversidad cultural, es un privilegio que responde a los sentidos más exquisitos del viajero curioso, aquél que gusta de asomarse a todos los matices que forman el alma y el corazón de los pueblos</p>
                </div>

                <div class="col-md-3 item">
                    <a href="{{route('destino')}}"><img class="img-fluid" src="img/chiapas/portada.jpg"></a>
                    <h3 class="name">Chiapas</h3>
                    <p class="description">Chiapas, un destino para hacer turismo cultural Visitar Chiapas, estado con gran diversidad cultural, es un privilegio que responde a los sentidos más exquisitos del viajero curioso, aquél que gusta de asomarse a todos los matices que forman el alma y el corazón de los pueblos</p>
                </div>
               
            </div>
            <div class="row articles">
                <div class="col-md-4 item" >
                    <a href="{{route('destino')}}"><img class="img-fluid" src="img/royal/portada.jpg"></a>
                    <h3 class="name">Royal Solaris Los Cabos</h3>
                    <p class="description">Royal Solaris los Cabos resort and SPA, se encuentra entre los mejores hoteles en Los Cabos México que ofrecen vacaciones todo incluido para familias. El hotel tiene instalaciones para toda la familia, ofrece excelente servicio, actividades durante el día en la piscina, espectáculos nocturnos y y un mini parque acuático infantil para los más pequeños.</p>
                </div>

                <div class="col-md-4 item" >
                    <a href="{{route('destino')}}"><img class="img-fluid" src="img/huasteca/portada.jpg"></a>
                    <h3 class="name">Huasteca Potosina</h3>
                    <p class="description">Un sitio esplendoroso por sus paisajes y exuberante vegetación, por sus ríos y espectaculares cascadas, por sus cuevas y profundos abismos, por supuesto por la magia de sus sitios arqueológicos y por la grandeza de sus etnias que al vivir sus costumbres y tradiciones prehispánicas nos remontan a un pasado que sigue presente en cada rincón de esta fascinante región de México</p>
                </div>

                <div class="col-md-4 item">
                    <a href="{{route('destino')}}"><img class="img-fluid" src="img/iberica/portada.jpg"></a>
                    <h3 class="name">Ronda Iberica</h3>
                    <p class="description">Este tour nos llevará por los lugares más espectaculares de la Península Ibérica! Comenzando por la Capitalidad de Madrid hacia el Mar Cantábrico de montañas verdes. Luego un recorrido por lo más visitado de Portugal.</p>
                </div>
            </div>
        </div>
    </div>


    <div>
        <div id="box-2">
            
            <h1 style="font-family: Amiri, serif;color: rgb(255,255,255);">Accede a  nuestras promociones
                <br>
                <a href="{{route('promociones')}}">
                    <button class="btn btn-info" type="button">Promociones</button>
                </a>
                
            </h1>

        </div>
    </div>

    <div class="article-list">
        <div class="container">
            <div class="intro">
                <h2 class="text-center" style="color: rgb(20,133,238);">Blog</h2>
                <p class="text-center">Nunc luctus in metus eget fringilla. Aliquam sed justo ligula. Vestibulum nibh erat, pellentesque ut laoreet vitae. </p>
            </div>

            <div class="row articles">
                <div class="col-sm-6 col-md-4 item">
                    <a href="{{route('entrada')}}"><img class="img-fluid" src="img/ChiquiClub.jpg">
                        <h3 class="name">Los pequeños tienen su espacio</h3>
                        <p class="description">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est, interdum justo suscipit id.</p>
                    </a>
                    {{-- <a href="{{route('entrada')}}" class="action">
                        <button class="btn btn-primary center-block" type="button" style="background-color: rgb(62,140,228);">Leer</button>
                    </a> --}}
                    
                </div>
 
                <div class="col-sm-6 col-md-4 item text-center">
                    <a href="{{route('entrada')}}"><img class="img-fluid" src="img/frutas.jpg">
                        <h3 class="name">Renuevan alberca en hotel HH</h3>
                        <p class="description">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est, interdum justo suscipit id.</p>
                    </a>
                    {{-- <a href="{{route('entrada')}}" class="action">
                        <button class="btn btn-primary center-block" type="button"style="background-color: rgb(62,140,228);">Leer</button>
                    </a> --}}
                </div>
                
                <div class="col-sm-6 col-md-4 item text-center">
                    <a href="{{route('entrada')}}"><img class="img-fluid" src="img/Fachada.jpg">
                        <h3 class="name">Holiday inn abre sus puertas nuevamente</h3>
                        <p class="description">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est, interdum justo suscipit id.</p>
                    </a>

                 {{--    <a href="{{route('entrada')}}">                        
                        <button class="btn btn-primary" type="button" style="background-color: rgb(62,140,228);">Leer</button>
                    </a> --}}
                </div>
            </div>
        </div>
    </div>