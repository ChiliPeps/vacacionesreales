@extends('cms.auth.master')

@section('content')

    <div class="login-box" id="appname" v-cloak>

        <div class="login-logo">
            <a href="#"><b>{{ config('cms.app_name') }}</b> CMS</a>
        </div> <!-- /.login-logo -->

        {{-- Errors --}}
        @if($errors->any())
            <div class="alert alert-danger">
                <h5><i class="icon fa fa-ban"></i> Error, algunos valores no son válidos</h5>
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Accede con tus credenciales para iniciar sesión</p>

                {!! Form::open(['route' => 'admin.login', 'method' => 'post']) !!}
                <div class="input-group mb-3">
                    {!! Form::email('email', null, ['class' => CMSHelpers::checkInput($errors, 'email'), 'placeholder' => 'Email']) !!}
                    <div class="input-group-append">
                        <div class="input-group-text"><span class="fas fa-envelope"></span></div>
                    </div>
                </div>
                <div class="input-group {{ $errors->has('password') ? 'has-error' : '' }} mb-3">
                    {!! Form::password('password', ['class' => CMSHelpers::checkInput($errors, 'password'), 'placeholder' => 'Password']) !!}
                    <div class="input-group-append">
                        <div class="input-group-text"><span class="fas fa-lock"></span></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-8">
                        <div class="icheck-primary">
                            <input type="checkbox" id="remember" name="remember" {{ old('remember') ? 'checked' : ''}}> <label for="remember">Recuerdame</label>
                        </div>
                    </div> <!-- /.col -->
                    <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block">Acceder</button>
                    </div> <!-- /.col -->
                </div>
                {!! Form::close() !!}
    
                <p class="mb-1">
                    <a href="{{ route('admin.recover-password') }}">Olvidé mi contraseña</a>
                </p>

                {{-- <div class="social-auth-links text-center mb-3">
                    <a href="{{url('/')}}" class="btn btn-block btn-primary">
                        <i class="fas fa-arrow-circle-left mr-2"></i> Regresar al Inicio
                    </a>
                    <a href="#" class="btn btn-block btn-danger">
                        <i class="fab fa-google-plus mr-2"></i> 
                    </a>
                </div> --}}

            </div> <!-- /.login-card-body -->


           

        </div>

    </div> <!-- /.login-box -->
@endsection