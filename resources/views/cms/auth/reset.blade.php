@extends('cms.auth.master')

@section('content')
    <div class="login-box">
        <div class="login-logo">
            <a href="#"><b>{{ config('cms.app_name') }}</b> CMS</a>
        </div><!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Recuperar contraseña</p>

            {{-- Errors --}}
            @if($errors->any())
                <div class="alert alert-danger">
                    <h5><i class="icon fa fa-ban"></i> Error, algunos valores no son válidos</h5>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {{-- Errors --}}

            {!! Form::open(['route' => 'admin.reset-password', 'method' => 'post']) !!}
                <input type="hidden" name="token" value="{{ $token }}">

                <div class="input-group mb-3 has-feedback">
                    {!! Form::email('password', null, ['class' => CMSHelpers::checkInput($errors, 'password'), 'placeholder' => 'Password']) !!}
                    <div class="input-group-append">
                        <div class="input-group-text"><span class="fas fa-envelope"></span></div>
                    </div>
                </div>

                <div class="input-group mb-3 has-feedback">
                    {!! Form::email('password_confirmation', null, ['class' => CMSHelpers::checkInput($errors, 'password_confirmation'), 'placeholder' => 'Password']) !!}
                    <div class="input-group-append">
                        <div class="input-group-text"><span class="fas fa-envelope"></span></div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-xs-6">
                    </div><!-- /.col -->
                    <div class="col-xs-6">
                        <button type="submit" class="btn btn-primary btn-block">Crear nueva contraseña</button>
                    </div><!-- /.col -->
                </div>
            {!! Form::close() !!}

            <p class="mt-3 mb-1">
                <a href="{{ route('admin.login') }}">Regresar al Login</a>
            </p> <br>

        </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
@endsection
