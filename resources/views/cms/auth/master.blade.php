<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>{{ config('cms.app_name') }}</title>
    <meta id="token" name="csrf-token" content="{{ csrf_token() }}">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="public_url" content="{{ url('/') }}/" />
    <meta name="app_url" content="{{ env('CMS_APP_BASE_URL') }}" />
    
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/cms.css') }}" rel="stylesheet">

    {!! Html::favicon('favicon.ico') !!}

    @stack('css')

    {{-- Background Image --}}
    @php
        $loginBackground = CMSHelpers::getCMSLoginBackground();
    @endphp

    @if($loginBackground != 'null')
    <style>
    .login-page, .register-page {
        background-image: url("{{ url('/').'/storage/'.$loginBackground }}");
        background-size: cover;
        background-position: center;
    }
    </style>
    @endif


</head>
<body class="hold-transition login-page">

    @yield('content')

    <div id="app"></div>

    {{-- Footer JS Scripts --}}
    <script src="{{ asset('js/manifest.js') }}" defer></script>
    <script src="{{ asset('js/cms_vendor.js') }}" defer></script>
    {{-- <script src="{{ asset('js/cms.js') }}" defer></script> --}}
    
    @stack('scripts')

</body>
</html>