@extends('cms.auth.master')

@section('content')

<div class="login-box">
    <div class="login-logo">
        <a href="#"><b>{{ config('cms.app_name') }}</b> CMS</a>
    </div> <!-- /.login-logo -->
    
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Olvidaste tu contraseña? Aquí puedes solicitar una nueva.</p>

            {{-- Errors --}}
            @if($errors->any())
                <div class="alert alert-danger">
                    <h5><i class="icon fa fa-ban"></i> Error, algunos valores no son válidos</h5>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {{-- Errors --}}

            @if(Session::has('status'))
                <div class="alert alert-success" role="alert">
                    <p>Te hemos enviado tu liga para reiniciar tu contraseña!</p>
                </div>
            @endif

            {!! Form::open(['route' => 'admin.recover-password', 'method' => 'post']) !!}
                <div class="input-group mb-3">
                    {!! Form::email('email', null, ['class' => CMSHelpers::checkInput($errors, 'email'), 'placeholder' => 'E-mail']) !!}
                    <div class="input-group-append">
                        <div class="input-group-text"><span class="fas fa-envelope"></span></div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary btn-block">Solicitar nueva contraseña</button>
                    </div> <!-- /.col -->
                </div>
            {!! Form::close() !!}
  
            <p class="mt-3 mb-1">
                <a href="{{ route('admin.login') }}">Regresar al Login</a>
            </p>

        </div> <!-- /.login-card-body -->
    </div>
</div> <!-- /.login-box -->

@endsection













