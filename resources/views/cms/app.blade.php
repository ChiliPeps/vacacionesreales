<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>{{ config('cms.app_name') }}</title>
    <meta id="token" name="csrf-token" content="{{ csrf_token() }}">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="public_url" content="{{ url('/') }}/" />
    <meta name="app_url" content="{{ env('CMS_APP_BASE_URL') }}" />
    <meta name="app_name" content="{{ env('APP_NAME') }}" />
    
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/cms.css') }}" rel="stylesheet">

    {!! Html::favicon('favicon.ico') !!}

</head>
<body class="{{ CMSHelpers::cmsBodyClass() }}">

    {{-- Vue App --}}
    <div id="app" class="wrapper">
        <navtop></navtop>
        <sidebar></sidebar>
        <transition :name="getPrincipalTransition" mode="out-in">
            <router-view></router-view>
        </transition>
        <footbot></footbot>
    </div>

    {{-- Footer JS Scripts --}}
    <script src="{{ asset('js/manifest.js') }}" defer></script>
    <script src="{{ asset('js/cms_vendor.js') }}" defer></script>
    <script src="{{ asset('js/cms.js') }}" defer></script>

</body>
</html>