{{-- jquery --}}
{!! Html::script('js/bootstrapstudio/jquery.min.js') !!}
{{-- bootstrap --}}
{!! Html::script('js/bootstrap.min.js') !!}
{{-- {!! Html::script('js/bootstrapv4.2.1.min.js') !!} --}}

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>

{{-- BootstrapStudio --}}
{!! Html::script('js/bootstrapstudio/agency.js') !!}
{{-- {!! Html::script('js/bootstrapstudio/Filterable-Gallery-with-Lightbox.js') !!} --}}
{!! Html::script('js/bootstrapstudio/Simple-Slider.js') !!}

{{-- ajusta el jumbotron despues del navbar --}}
{{-- {!! Html::script('js/bootstrapstudio/size.js') !!} --}}


<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>

{{-- Stack Scripts --}}
@stack('scripts')