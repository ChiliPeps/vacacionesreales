 <nav class="navbar navbar-expand-lg fixed-top" id="mainNav" style="background-color:rgba(124,187,229,0.8);">
    <div class="container">
        <a class="navbar-brand" href="{{route('inicio')}}">
            <div class="logo">
                {{-- <img src=asset('img/logo.png')/> --}}
                <img src="{{asset('img/logo1.png')}}">
            </div>
        </a>
        <button class="navbar-toggler navbar-toggler-right" data-toggle="collapse" data-target="#navbarResponsive" type="button" data-toogle="collapse" aria-controls="navbarResponsive"
            aria-expanded="false" aria-label="Toggle navigation">
            <i class="fa fa-bars"></i>
        </button>

        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="nav navbar-nav ml-auto text-uppercase">
                <li class="nav-item" role="presentation">
                    <a href="{{route('inicio')}}" class="nav-link js-scroll-trigger">Inicio</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a href="{{route('hoteles')}}" class="nav-link">Hoteles</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a href="{{route('promociones')}}" class="nav-link">Promociones</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a href="{{route('galeria')}}" class="nav-link">Galería</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a href="{{route('blog')}}" class="nav-link">Blog</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a href="{{route('contacto')}}" class="nav-link">Contact</a>
                </li>
            </ul>
        </div>
    </div>
</nav>