{{-- bootstrap --}}
{{-- {!! Html::style('css/bootstrap.min.css') !!} --}}
{!! Html::style('css/bootstrapv4.2.1.min.css') !!}

{{-- bootstrapstudio --}}
{!! Html::style('css/bootstrapstudio/stylo.css') !!}
{!! Html::style('css/bootstrapstudio/Social-Icons.css') !!}
{!! Html::style('css/bootstrapstudio/Simple-Slider.css') !!}
{!! Html::style('css/bootstrapstudio/Parallax-Scroll-Effect.css') !!}
{!! Html::style('css/bootstrapstudio/Newsletter-Subscription-Form.css') !!}
{!! Html::style('css/bootstrapstudio/Map-Clean.css') !!}
{!! Html::style('css/bootstrapstudio/Lightbox-Gallery.css') !!}
{!! Html::style('css/bootstrapstudio/Highlight-Clean.css') !!}
{!! Html::style('css/bootstrapstudio/Highlight-Blue.css') !!}
{!! Html::style('css/bootstrapstudio/Hero-Photography.css') !!}
{!! Html::style('css/bootstrapstudio/Footer-Dark.css') !!}
{!! Html::style('css/bootstrapstudio/Filterable-Gallery-with-Lightbox.css') !!}

{!! Html::style('css/bootstrapstudio/Features-Boxed.css') !!}
{!! Html::style('css/bootstrapstudio/Features-Clean.css') !!}

{{-- {!! Html::style('css/bootstrapstudio/clean-responsive-pricing-1.css') !!} --}}
{!! Html::style('css/bootstrapstudio/clean-responsive-pricing.css') !!}
{!! Html::style('css/bootstrapstudio/Brands.css') !!}
{!! Html::style('css/bootstrapstudio/Article-List.css') !!}




<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/css/lightbox.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css">


{{-- fonts --}}
{!! Html::style('fonts/fontawesome-all.min.css') !!}
{!! Html::style('fonts/font-awesome.min.css') !!}
{!! Html::style('fonts/ionicons.min.css') !!}


{{-- Stack for CSS --}}
@stack('css')