import Vue from 'vue'
import router from './router'
import store from './store'
import config from './config'

// AdminLTE + Dependencies
window.$ = window.jQuery = require('jquery')
require('admin-lte')
require('bootstrap')
require('icheck-bootstrap')

// Get App Public Url & App Name
Vue.prototype.$publicUrl = config.publicUrl
Vue.prototype.$appName = config.appName
Vue.prototype.$token = config.token

// SWAL Toasts
import Swal from 'sweetalert2'
Vue.prototype.$toast = Swal.mixin({
	toast: true,
	position: 'top',
	showConfirmButton: false,
	timer: 5000,
	timerProgressBar: true
})

Vue.config.productionTip = false

// Initial Components
import navtop from './components/navtop'
import sidebar from './components/sidebar'
import footbot from './components/footbot'

new Vue({
	el: '#app',
	router,
	store,
	components: { navtop, sidebar, footbot },
	computed: {
		getPrincipalTransition() {
			return this.$store.getters['configuration/getPrincipalTransition']
		}
	}
})