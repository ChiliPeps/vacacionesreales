import Vue from 'vue'
import VueRouter from 'vue-router'

// PHP .env VUE_APP_BASE_URL
const appUrl = document.head.querySelector('meta[name="app_url"]').getAttribute('content')

// Modules Routes
var routes = []
import home from './modules/home/routes'
import search from './modules/search/routes'
import users from './modules/users/routes'
import categories from './modules/categories/routes'
import blog from './modules/blog/routes'
import gallery from './modules/gallery/routes'
import configuration from './modules/configuration/routes'

// Juntar las Rutas
routes = routes.concat(home, search, users, categories, blog, gallery, configuration)
Vue.use(VueRouter)

const router = new VueRouter({
    //mode: 'history', //removes # (hashtag) from url
    base: appUrl,
    fallback: true, //router should fallback to hash (#) mode when the browser does not support history.pushState
    routes,
    scrollBehavior() {
        return { x:0, y:0 }
    }
});

export default router
