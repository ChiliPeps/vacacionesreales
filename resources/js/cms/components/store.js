import api from '../api'

export default {
    namespaced: true,
    state: {
        user_info: null,
        menu: [
            { id: 0, text: 'Inicio', route: '/', icon: 'fas fa-tachometer-alt' },
            { id: 1, text: 'Usuarios', route: '/usuarios/index', icon: 'fas fa-users' },
            { id: 2, text: 'Categorías', route: '/categorias/index', icon: 'fas fa-archive' },
            { id: 3, text: 'Blog', route: '/blog/index', icon: 'fas fa-newspaper' },
            { id: 4, text: 'Galería', route: '/galeria/index', icon: 'fas fa-images' },
            { id: 5, text: 'Configuración', route: '/configuracion/edit', icon: 'fas fa-cogs' }
        ],
        bread: { titulo: '', text: [], icon: '' }
    },
    mutations: {
        setBread: (state, payload) => { state.bread = payload },
        setUserInfo: (state, payload) => { state.user_info = payload },
    },
    getters: {
        hasDataUserInfo: state => {
            if (state.user_info == null) { return false }
            else { return true }
        },
        getUserInfo: state => { return state.user_info },
        getUserFullName: state => { 
            if (state.user_info == null) { return '' }
            return state.user_info.nombre + ' ' + state.user_info.apellido
        },
        getBread: state => { return state.bread },
        getMenu: state => { return state.menu },
    },
    actions: {
        getCMSUserInfo: ({ commit }) => {
            return api.get('/cms/user').then((response) => {
                commit('setUserInfo', response.data)
                return response
            }).catch((error) => {
                console.log(error)
                throw error
            })
        }
    }
}
