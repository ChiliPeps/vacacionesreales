import api from '../../api'

export default {
    namespaced: true,
    state: {
        loadingConfiguration: false,
        configuration: {
            template_layout_options: 'null',
            navbar_variants: 'navbar-light navbar-white',
            accent_color_variants: 'null',
            sidebar_variants: 'sidebar-dark-primary',
            brand_logo_variants: 'null',
            login_background_url: 'null',
            correo_contacto: '',
            front_site_up: '0',
            buscador_header: '1',
            principal_transition: 'fade',
            secondary_transition: 'fade',
            shortcuts: [],
            logo_cms: 'null'
        },
        template_layouts: [
            { text: 'None', value: 'null' },
            { text: 'Layout Fixed', value: 'layout-fixed' },
            { text: 'Layout Navbar Fixed', value: 'layout-navbar-fixed' },
            { text: 'Layout Footer Fixed', value: 'layout-footer-fixed' },
            { text: 'Sidebar Collapse', value: 'sidebar-collapse' },
            { text: 'Layout Boxed', value: 'layout-boxed' },
            //{ text: 'Layout Top Nav', value: 'layout-top-nav' }
        ],
        navbar_variants: [
            { text: 'Navbar White', value: 'navbar-light navbar-white' },
            { text: 'Navbar Gray Dark', value: 'navbar-dark navbar-gray-dark' },
            { text: 'Navbar Gray', value: 'navbar-dark navbar-gray' },
            { text: 'Navbar Cyan', value: 'navbar-dark navbar-cyan' },
            { text: 'Navbar Teal', value: 'navbar-dark navbar-teal' },
            { text: 'Navbar Green', value: 'navbar-dark navbar-green' },
            { text: 'Navbar Yellow', value: 'navbar-light navbar-yellow' },
            { text: 'Navbar Orange', value: 'navbar-light navbar-orange' },
            { text: 'Navbar Red', value: 'navbar-light navbar-red' },
            { text: 'Navbar Pink', value: 'navbar-dark navbar-pink' },
            { text: 'Navbar Purple', value: 'navbar-dark navbar-purple' },
            { text: 'Navbar Indigo', value: 'navbar-dark navbar-indigo' },
            { text: 'Navbar Blue', value: 'navbar-dark navbar-blue' },
            { text: 'Navbar Maroon', value: 'navbar-dark navbar-maroon' },
            { text: 'Navbar Fuchsia', value: 'navbar-light navbar-fuchsia' },
            { text: 'Navbar Lime', value: 'navbar-light navbar-lime' },
            { text: 'Navbar Olive', value: 'navbar-dark navbar-olive' },
            { text: 'Navbar Navy', value: 'navbar-dark navbar-navy' },
            { text: 'Navbar Lightblue', value: 'navbar-dark navbar-lightblue' },
            { text: 'Navbar Danger', value: 'navbar-light navbar-danger' },
            { text: 'Navbar Warning', value: 'navbar-light navbar-warning' },
            { text: 'Navbar Info', value: 'navbar-dark navbar-info' },
            { text: 'Navbar Success', value: 'navbar-dark navbar-success' },
            { text: 'Navbar Secondary', value: 'navbar-dark navbar-secondary' },
            { text: 'Navbar Primary', value: 'navbar-dark navbar-primary' },
            { text: 'Navbar Dark', value: 'navbar-dark' },
            { text: 'Navbar Light', value: 'navbar-light' }
        ],
        accent_color_variants: [
            { text: 'None', value: 'null' },
            { text: 'Accent Primary', value: 'accent-primary' },
            { text: 'Accent Warning', value: 'accent-warning' },
            { text: 'Accent Info', value: 'accent-info' },
            { text: 'Accent Danger', value: 'accent-danger' },
            { text: 'Accent Success', value: 'accent-success' },
            { text: 'Accent Indigo', value: 'accent-indigo' },
            { text: 'Accent Navy', value: 'accent-navy' },
            { text: 'Accent Purple', value: 'accent-purple' },
            { text: 'Accent Fuchsia', value: 'accent-fuchsia' },
            { text: 'Accent Pink', value: 'accent-pink' },
            { text: 'Accent Maroon', value: 'accent-maroon' },
            { text: 'Accent Orange', value: 'accent-orange' },
            { text: 'Accent Lime', value: 'accent-lime' },
            { text: 'Accent Teal', value: 'accent-teal' },
            { text: 'Accent Olive', value: 'accent-olive' }
        ],
        sidebar_variants: [
            { text: 'Dark Sidebar Primary', value: 'sidebar-dark-primary' },
            { text: 'Dark Sidebar Warning', value: 'sidebar-dark-warning' },
            { text: 'Dark Sidebar Info', value: 'sidebar-dark-info' },
            { text: 'Dark Sidebar Danger', value: 'sidebar-dark-danger' },
            { text: 'Dark Sidebar Success', value: 'sidebar-dark-success' },
            { text: 'Dark Sidebar Indigo', value: 'sidebar-dark-indigo' },
            { text: 'Dark Sidebar Navy', value: 'sidebar-dark-navy' },
            { text: 'Dark Sidebar Purple', value: 'sidebar-dark-purple' },
            { text: 'Dark Sidebar Fuchsia', value: 'sidebar-dark-fushsia' },
            { text: 'Dark Sidebar Pink', value: 'sidebar-dark-pink' },
            { text: 'Dark Sidebar Maroon', value: 'sidebar-dark-maroon' },
            { text: 'Dark Sidebar Orange', value: 'sidebar-dark-orange' },
            { text: 'Dark Sidebar Lime', value: 'sidebar-dark-lime' },
            { text: 'Dark Sidebar Teal', value: 'sidebar-dark-teal' },
            { text: 'Dark Sidebar Olive', value: 'sidebar-dark-olive' },
            { text: 'Light Sidebar Primary', value: 'sidebar-light-primary' },
            { text: 'Light Sidebar Warning', value: 'sidebar-light-warning' },
            { text: 'Light Sidebar Info', value: 'sidebar-light-info' },
            { text: 'Light Sidebar Danger', value: 'sidebar-light-danger' },
            { text: 'Light Sidebar Success', value: 'sidebar-light-success' },
            { text: 'Light Sidebar Indigo', value: 'sidebar-light-indigo' },
            { text: 'Light Sidebar Navy', value: 'sidebar-light-navy' },
            { text: 'Light Sidebar Purple', value: 'sidebar-light-purple' },
            { text: 'Light Sidebar Fuchsia', value: 'sidebar-light-fushsia' },
            { text: 'Light Sidebar Pink', value: 'sidebar-light-pink' },
            { text: 'Light Sidebar Maroon', value: 'sidebar-light-maroon' },
            { text: 'Light Sidebar Orange', value: 'sidebar-light-orange' },
            { text: 'Light Sidebar Lime', value: 'sidebar-light-lime' },
            { text: 'Light Sidebar Teal', value: 'sidebar-light-teal' },
            { text: 'Light Sidebar Olive', value: 'sidebar-light-olive' },
        ],
        brand_logo_variants: [
            { text: 'None', value: 'null' },
            { text: 'Brand Logo White', value: 'navbar-white' },
            { text: 'Brand Logo Gray Dark', value: 'navbar-gray-dark' },
            { text: 'Brand Logo Gray', value: 'navbar-gray' },
            { text: 'Brand Logo Cyan', value: 'navbar-cyan' },
            { text: 'Brand Logo Teal', value: 'navbar-teal' },
            { text: 'Brand Logo Green', value: 'navbar-green' },
            { text: 'Brand Logo Yellow', value: 'navbar-yellow' },
            { text: 'Brand Logo Orange', value: 'navbar-orange' },
            { text: 'Brand Logo Red', value: 'navbar-red' },
            { text: 'Brand Logo Pink', value: 'navbar-pink' },
            { text: 'Brand Logo Purple', value: 'navbar-purple' },
            { text: 'Brand Logo Indigo', value: 'navbar-indigo' },
            { text: 'Brand Logo Blue', value: 'navbar-blue' },
            { text: 'Brand Logo Maroon', value: 'navbar-maroon' },
            { text: 'Brand Logo Fuchsia', value: 'navbar-fushsia' },
            { text: 'Brand Logo Lime', value: 'navbar-lime' },
            { text: 'Brand Logo Olive', value: 'navbar-olive' },
            { text: 'Brand Logo Navy', value: 'navbar-navy' },
            { text: 'Brand Logo Lightblue', value: 'navbar-lightblue' },
            { text: 'Brand Logo Danger', value: 'navbar-danger' },
            { text: 'Brand Logo Warning', value: 'navbar-warning' },
            { text: 'Brand Logo Info', value: 'navbar-info' },
            { text: 'Brand Logo Success', value: 'navbar-success' },
            { text: 'Brand Logo Secondary', value: 'navbar-secondary' },
            { text: 'Brand Logo Primary', value: 'navbar-primary' },
            { text: 'Brand Logo Dark', value: 'navbar-dark' },
            { text: 'Brand Logo Light', value: 'navbar-light' }
        ],
        transitions: [
            { text: 'Bounce', value: 'bounce' },
            { text: 'Bounce Down', value: 'bounceDown' },
            { text: 'Bounce Left', value: 'bounceLeft' },
            { text: 'Bounce Right', value: 'bounceRight' },
            { text: 'Bounce Up', value: 'bounceUp' },
            { text: 'Fade', value: 'fade' },
            { text: 'Fade Down', value: 'fadeDown' },
            { text: 'Fade Down Big', value: 'fadeDownBig' },
            { text: 'Fade Left', value: 'fadeLeft' },
            { text: 'Fade Left Big', value: 'fadeLeftBig' },
            { text: 'Fade Right', value: 'fadeRight' },
            { text: 'Fade Right Big', value: 'fadeRightBig' },
            { text: 'Fade Up', value: 'fadeUp' },
            { text: 'Fade Up Big', value: 'fadeUpBig' },
            { text: 'Flip', value: 'flip' },
            { text: 'Flip X', value: 'flipX' },
            { text: 'Flip Y', value: 'flipY' },
            { text: 'Rotate', value: 'rotate' },
            { text: 'Rotate Down Left', value: 'rotateDownLeft' },
            { text: 'Rotate Down Right', value: 'rotateDownRight' },
            { text: 'Rotate Up Left', value: 'rotateUpLeft' },
            { text: 'Rotate Up Right', value: 'rotateUpRight' },
            { text: 'Slide Down', value: 'slideDown' },
            { text: 'Slide Left', value: 'slideLeft' },
            { text: 'slide Right', value: 'slideRight' },
            { text: 'Slide Up', value: 'slideUp' },
            { text: 'Zoom', value: 'zoom' },
            { text: 'Zoom Down', value: 'zoomDown' },
            { text: 'Zoom Left', value: 'zoomLeft' },
            { text: 'Zoom Right', value: 'zoomRight' },
            { text: 'Zoom Up', value: 'zoomUp' },
            { text: 'Light Speed', value: 'lightSpeed' }
        ],
        bread: {
            'Configuraciones': { title: 'Configuración del CMS', sections: ['Configuración'],  icon: 'fas fa-cogs' }
        },
        contactoErrors: []
    },
    mutations: {
        setConfiguration: (state, payload) => { state.configuration = payload },
        setAllConfiguration: (state, payload) => {
            state.configuration = {
                template_layout_options: payload.template_layout_options,
                navbar_variants: payload.navbar_variants,
                accent_color_variants: payload.accent_color_variants,
                sidebar_variants: payload.sidebar_variants,
                brand_logo_variants: payload.brand_logo_variants,
                login_background_url: payload.login_background_url,
                correo_contacto: payload.correo_contacto,
                front_site_up: payload.front_site_up,
                buscador_header: payload.buscador_header,
                principal_transition: payload.principal_transition,
                secondary_transition: payload.secondary_transition,
                shortcuts: JSON.parse(payload.shortcuts),
                logo_cms: payload.logo_cms
            }
        },

        // Colores
        setTemplateLayoutOptions: (state, payload) => { state.configuration.template_layout_options = payload },
        setNavbarVariants: (state, payload) => { state.configuration.navbar_variants = payload },
        setAccentColorVariants: (state, payload) => { state.configuration.accent_color_variants = payload },
        setSidebarVariants: (state, payload) => { state.configuration.sidebar_variants = payload },
        setBrandLogoVariants: (state, payload) => { state.configuration.brand_logo_variants = payload },

        // Loginback
        setLoginBackgroundUrl: (state, payload) => { state.configuration.login_background_url = payload },

        // Contacto
        setCorreoContacto: (state, payload) => { state.configuration.correo_contacto = payload },

        // Options
        setFrontSiteUp: (state, payload) => { state.configuration.front_site_up = payload },
        setBuscadorHeader: (state, payload) => { state.configuration.buscador_header = payload },

        // Efectos
        setPrincipalTransition: (state, payload) => { state.configuration.principal_transition = payload },
        setSecondaryTransition: (state, payload) => { state.configuration.secondary_transition = payload },

        // Shortcuts
        setShortcuts: (state, payload) => { state.configuration.shortcuts = payload },

        // Logo CMS
        setLogoCMS: (state, payload) => { state.configuration.logo_cms = payload },

        // << Form Errors >>
        setContactoErrors: (state, payload) => { state.contactoErrors = payload }
    },
    getters: {
        getBread: state => { return state.bread },
        getConfiguration: state => { return state.configuration },

        // Colores
        getTemplateLayoutOptions: state => { return state.configuration.template_layout_options },
        getNavbarVariants: state => { return state.configuration.navbar_variants },
        getAccentColorVariants: state => { return state.configuration.accent_color_variants },
        getSidebarVariants: state => { return state.configuration.sidebar_variants },
        getBrandLogoVariants: state => { return state.configuration.brand_logo_variants },
        getColores: state => { 
            return {
                template_layout_options: state.configuration.template_layout_options,
                navbar_variants: state.configuration.navbar_variants,
                accent_color_variants: state.configuration.accent_color_variants,
                sidebar_variants: state.configuration.sidebar_variants,
                brand_logo_variants: state.configuration.brand_logo_variants
            } 
        },

        // Loginback
        getLoginBackgroundUrl: state => { return state.configuration.login_background_url },
        hasLoginBackgroundUrl: state => { if (state.configuration.login_background_url == 'null') { return false } else { return true } },

        // Contacto
        getCorreoContacto: state => { return state.configuration.correo_contacto },

        // Options
        getFrontSiteUp: state => { return state.configuration.front_site_up },
        getBuscadorHeader: state => { return state.configuration.buscador_header },

        // Efectos
        getPrincipalTransition: state => { return state.configuration.principal_transition },
        getSecondaryTransition: state => { return state.configuration.secondary_transition },
        getEfectos: state => { 
            return {
                principal_transition: state.configuration.principal_transition,
                secondary_transition: state.configuration.secondary_transition
            } 
        },

        // Shortcuts
        getShortcuts: state => { return state.configuration.shortcuts },

        // Logo CMS
        getLogoCMS: state => { return state.configuration.logo_cms },
        hasLogoCMS: state => { if (state.configuration.logo_cms == 'null') { return false } else { return true } },

        // << Form Errors >>
        getContactoErrors: state => { return state.contactoErrors },

        // Array Options
        getTemplateLayoutOptionsArray: state => { return state.template_layouts },
        getNavbarVariantsArray: state => { return state.navbar_variants },
        getAccentColorVariantsArray: state => { return state.accent_color_variants },
        getSidebarVariantsArray: state => { return state.sidebar_variants },
        getBrandLogoVariantsArray: state => { return state.brand_logo_variants },
        getTransitionsArray: state => { return state.transitions }
    },
    actions: {
        getCMSConfiguration: ({ commit }) => {
            api.get('cms/get').then((response) => {
                commit('setAllConfiguration', response.data)
            }).catch((error) => {
                console.log(error)
            })
        },
        updateContacto: ({ getters, commit }) => {
            commit('setContactoErrors', [])
            return api.post('cms/contacto', { correo_contacto: getters.getCorreoContacto }).then((response) => {
                return response
            }).catch((error) => {
                console.log(error.message)
                commit('setContactoErrors', error.response.data.errors)
                throw error
            })
        },
        updateFrontSiteUp: ({ getters }) => {
            return api.post('cms/siteup', { front_site_up: getters.getFrontSiteUp }).then((response) => {
                return response
            }).catch((error) => {
                console.log(error.message)
                throw error
            })
        },
        updateBuscadorHeader: ({ getters }) => {
            return api.post('cms/buscador', { buscador_header: getters.getBuscadorHeader }).then((response) => {
                return response
            }).catch((error) => {
                console.log(error.message)
                throw error
            })
        },
        updateColores: ({ getters }) => {
            return api.post('cms/colores', getters.getColores).then((response) => {
                return response
            }).catch((error) => {
                console.log(error.message)
                throw error
            })
        },
        updateEfectos: ({ getters }) => {
            return api.post('cms/efectos', getters.getEfectos).then((response) => {
                return response
            }).catch((error) => {
                console.log(error.message)
                throw error
            })
        },
        updateShortcuts: ({ getters }) => {
            return api.post('cms/shortcuts', { shortcuts: JSON.stringify(getters.getShortcuts) }).then((response) => {
                return response
            }).catch((error) => {
                console.log(error.message)
                throw error
            })
        }
    }
}
