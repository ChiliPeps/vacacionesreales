// Componentes
import Base from './base'
import Inputs from './inputs'

export default [
    {
        path: '/configuracion',
        component: Base,
        children: [
            {
                name: 'Configuraciones',
                path: 'edit',
                component: Inputs
            }
        ]
    }
]