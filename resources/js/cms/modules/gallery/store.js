import api from '../../api'
import { findIndex } from 'lodash'
import tools from '../../tools'

export default {
    namespaced: true,
    state: {
        gallery: [],
        lightboxdata: [],
        pagination: null,
        bread: {
            'Lista de Imagenes': { title: 'Galería', sections: ['Galería'],  icon: 'fas fa-images' },
            'Subir Imagenes': { title: 'Subir Imagenes a la Galería', sections: ['Galería', 'Subir'], icon: 'fas fa-images' }
        }
    },
    mutations: {
        setGallery: (state, payload) => { state.gallery = payload },
        addImagesLightBoxPreview: (state) => {
            state.lightboxdata = []
            for (let i = 0; i < state.gallery.length; i++) {
                state.lightboxdata.push(tools.checkImage(state.gallery[i].imagen, true))
            }
        },
        setPagination: (state, payload) => { 
            state.pagination = payload
            delete state.pagination.data
        },
        addUploadedImage: (state, payload) => { state.gallery.push(payload) },
        setDeletedImage: (state, payload) => {
            let index = findIndex(state.gallery, (o) => { return o.id == payload.id })
            state.gallery.splice(index, 1)
            state.lightboxdata.splice(index, 1)
        }
    },
    getters: {
        getBread: state => { return state.bread },
        hasDataGallery: state => {
            if (state.gallery.length == 0) { return false }
            else { return true }
        },
        getGallery: state => { return state.gallery },
        getPagination: state => { return state.pagination },
        getLightBoxData: state => { return state.lightboxdata }
    },
    actions: {
        getGaleria: ({ commit }, params) => {
            var route = (params.url === "" || params.url === null) ? '/galeria/get' : params.url
            api.get(route).then((response) => {
                commit('setGallery', response.data.data)
                commit('addImagesLightBoxPreview')
                commit('setPagination', response.data)
            }).catch((error) => {
                console.log(error)
            })
        },
        deleteImagen: ({ commit }, id_imagen) => {
            return api.delete('galeria/delete', { params: { id:id_imagen } }).then((response) => {
                commit('setDeletedImage', response.data)
                return response
            }).catch((error) => {
                console.log(error.message)
                throw error
            })
        }
    }
}
