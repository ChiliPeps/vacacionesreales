// Componentes
import Base from './base'
import Index from './index'
import Imagenes from './imagenes'

export default [
    {
        path: '/galeria',
        component: Base,
        children: [
            {
                name: 'Lista de Imagenes',
                path: 'index',
                component: Index
            },
            {
                name: 'Subir Imagenes',
                path: 'upload',
                component: Imagenes,
            }
        ]
    }
]