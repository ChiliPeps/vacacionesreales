import api from '../../api'
import { findIndex } from 'lodash'
import tools from '../../tools'

export default {
    namespaced: true,
    state: {
        updateOn: false,
        post: {
            id: '',
            titulo: '',
            fecha: tools.getStringTodayDate(),
            descripcion: '',
            contenido: '<p>Inspirate y desarrolla tu contenido...</p>',
            autor: '',
            slugurl: '',
            imagen: '',
            thumb: '',
            autor_foto: '',
            publish: 0,
            id_categoria: null
        },
        posts: [],
        pagination: null,
        filtro: {
            selected: 'titulo',
            busqueda: '',
            opciones: [
                { text: 'Título', value: 'titulo', type: 'text' },
                { text: 'Categoría', value: 'categoria', type: 'text' },
                { text: 'Fecha', value: 'fecha', type: 'date' },
                { text: 'Descripción', value: 'descripcion', type: 'text' },
                { text: 'Autor', value: 'autor', type: 'text' },
                { text: 'Visitas', value: 'visitas', type: 'text' },
                { text: 'Publicado', value: 'publish', type: 'bool' }
            ]
        },
        postErrors: [],
        bread: {
            'Lista de Posts': { title: 'Entradas del Blog', sections: ['Blog'],  icon: 'fas fa-newspaper' },
            'Crear Post': { title: 'Crear Entrada', sections: ['Blog', 'Crear'], icon: 'fas fa-newspaper' },
            'Editar Post': { title: 'Actualizar Entrada', sections: ['Blog', 'Actualizar'], icon: 'fas fa-newspaper' }
        },
        categorias: [],
        loading: false,
        tinyImages: []
    },
    mutations: {
        setUpdateOn: (state, payload) => { state.updateOn = payload },
        setPosts: (state, payload) => { state.posts = payload },
        setPagination: (state, payload) => { 
            state.pagination = payload
            delete state.pagination.data
        },
        setFiltros: (state, payload) => {
            state.filtro.selected = payload.selected
            state.filtro.busqueda = payload.busqueda
        },
        setPostErrors: (state, payload) => { state.postErrors = payload },
        setPostAll: (state, payload) => { 
            state.post = {
                id: payload.id,
                titulo: payload.titulo,
                fecha: payload.fecha,
                descripcion: payload.descripcion,
                contenido: payload.contenido,
                autor: payload.autor,
                slugurl: payload.slugurl,
                imagen: payload.imagen,
                autor_foto: payload.autor_foto,
                publish: payload.publish,
                id_categoria: payload.id_categoria
            }
        },
        setPostId: (state,payload) => { state.post.id = payload },
        setPostTitulo: (state, payload) => { state.post.titulo = payload },
        setPostFecha: (state, payload) => { state.post.fecha = payload },
        setPostDescripcion: (state, payload) => { state.post.descripcion = payload },
        setPostContenido: (state, payload) => { state.post.contenido = payload },
        setPostAutor: (state, payload) => { state.post.autor = payload },
        setPostSlugurl: (state, payload) => { state.post.slugurl = payload },
        setPostImagen: (state, payload) => { state.post.imagen = payload },
        setPostThumb: (state, payload) => { state.post.thumb = payload },
        setPostAutorFoto: (state, payload) => { state.post.autor_foto = payload },
        setPostPublish: (state, payload) => { state.post.publish = payload },
        setPostIdCategoria: (state, payload) => { state.post.id_categoria = payload },
        setCategorias: (state, payload) => { state.categorias = payload },
        setPublish: (state, payload) => { 
            let index = findIndex(state.posts, (o) => { return o.id == payload.id })
            state.posts[index].publish = payload.publish
        },
        setLoading: (state, payload) => { state.loading = payload },
        addTinyImage: (state, payload) => { state.tinyImages.push({ url: payload }) },
        addCategoria: (state, payload) => { state.categorias.push(payload) },
        cleanPost: (state) => {
            state.post = {
                id: '',
                titulo: '',
                fecha: tools.getStringTodayDate(),
                descripcion: '',
                contenido: '<p>Inspirate y desarrolla tu contenido...</p>',
                autor: '',
                slugurl: '',
                imagen: '',
                thumb: '',
                autor_foto: '',
                publish: 0,
                id_categoria: null
            }
        },
        setDeletePost: (state, payload) => {
            let index = findIndex(state.posts, (o) => { return o.id == payload.id })
            state.posts.splice(index, 1)
        }
    },
    getters: {
        getBread: state => { return state.bread },
        getUpdateOn: state => { return state.updateOn },
        hasDataPosts: state => { if (state.posts.length == 0) { return false } else { return true } },
        getPagination: state => { return state.pagination },
        getFiltros: state => { return state.filtro },
        getPostErrors: state => { return state.postErrors },
        getPosts: state => { return state.posts },
        getPost: state => { return state.post },
        getPostId: state => { return state.post.id },
        getPostTitulo: state => { return state.post.titulo },
        getPostFecha: state => { return state.post.fecha },
        getPostDescripcion: state => { return state.post.descripcion },
        getPostContenido: state => { return state.post.contenido },
        getPostAutor: state => { return state.post.autor },
        getPostSlugurl: state => { return state.post.slugurl },
        getPostImagen: state => { return state.post.imagen },
        getPostThumb: state => { return state.post.thumb },
        getPostAutorFoto: state => { return state.post.autor_foto },
        getPostPublish: state => { return state.post.publish },
        getPostIdCategoria: state => { return state.post.id_categoria },
        hasPostImagen: state => { if (state.post.imagen == '') { return false } else { return true } },
        getCategorias: state => { return state.categorias },
        getLoading: state => { return state.loading }
    },
    actions: {
        getPosts: ({ commit }, params) => {
            var route = (params.url === "" || params.url === null) ? '/blog/get' : params.url
            api.get(route, { params: { tipo: params.filtro.selected, busqueda: params.filtro.busqueda } }).then((response) => {
                commit('setPosts', response.data.data)
                commit('setPagination', response.data)
            }).catch((error) => {
                console.log(error)
            })
        },
        getListCategorias: ({ commit }) => {
            api.get('categorias/list').then((response) => {
                commit('setCategorias', response.data)
            }).catch((error) => {
                console.log(error)
            })
        },
        createPost: ({ commit, getters }) => {
            commit('setPostErrors', [])
            return api.post('blog/create', getters.getPost).then((response) => {
                commit('setPostAll', response.data)
                return response.data
            }).catch((error) => {
                console.log(error.message)
                commit('setPostErrors', error.response.data.errors)
                throw error
            })
        },
        getPost: ({ commit, getters }) => {
            return api.get('blog/get/' + getters.getPostId).then((response) => {
                commit('setPostAll', response.data)
                return response.data
            }).catch((error) => {
                console.log(error)
                throw error
            })
        },
        updatePost: ({ commit, getters }) => {
            commit('setPostErrors', [])
            return api.post('blog/update', getters.getPost).then((response) => {
                return response.data
            }).catch((error) => {
                console.log(error.message)
                commit('setPostErrors', error.response.data.errors)
                throw error
            })
        },
        deletePost: ({ commit }, id_post) => {
            return api.delete('blog/delete', { params: { id: id_post } }).then((response) => {
                commit('setDeletePost', response.data)
                return response
            }).catch((error) => {
                console.log(error.message)
                throw error
            })
        },
        togglePublish: ({ commit }, id_post) => {
            return api.post('blog/publish', { id: id_post }).then((response) => {
                commit('setPublish', response.data)
                return response
            }).catch((error) => {
                console.log(error.message)
                throw error
            })
        },
        uploadTinyImages: ({ commit }, formData) => {
            return api.post('blog/tinyimage', formData, {
                headers: { 'Content-Type': 'multipart/form-data' }
            }).then((response) => {
                commit('addTinyImage', response.data)
                return response
            }).catch((error) => {
                console.log(error.message)
                throw error.response.data.errors
            })
        },
        createCategoria: ({ commit }, nombre) => {
            return api.post('blog/categoria', { nombre: nombre }).then((response) => {
                commit('addCategoria', response.data)
                return response.data
            }).catch((error) => {
                console.log(error.message)
                throw error
            })
        }
    }
}
