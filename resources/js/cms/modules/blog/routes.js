// Componentes
import Base from './base'
import Index from './index'
import Inputs from './inputs'

export default [
    {
        path: '/blog',
        component: Base,
        children: [
            {
                name: 'Lista de Posts',
                path: 'index',
                component: Index
            },
            {
                name: 'Crear Post',
                path: 'create',
                component: Inputs,
            },
            {
                name: 'Editar Post',
                path: 'edit/:id',
                component: Inputs
            }
        ]
    }
]