// Componentes
import Base from './base'
import Index from './index'
import Inputs from './inputs'

export default [
    {
        path: '/usuarios',
        component: Base,
        children: [
            {
                name: 'Lista de Usuarios',
                path: 'index',
                component: Index
            },
            {
                name: 'Crear Usuario',
                path: 'create',
                component: Inputs,
            },
            {
                name: 'Editar Usuario',
                path: 'edit/:id',
                component: Inputs
            }
        ]
    }
]