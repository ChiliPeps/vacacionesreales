import api from '../../api'
import { findIndex } from 'lodash'

export default {
    namespaced: true,
    state: {
        updateOn: false,
        users: [],
        pagination: null,
        filtro: {
            selected: 'nombre',
            busqueda: '',
            opciones: [
                { text: 'Nombre', value: 'nombre', type: 'text' },
                { text: 'Apellido', value: 'apellido', type: 'text' },
                { text: 'Email', value: 'email', type: 'text' },
                { text: 'Tipo', value: 'type', type: 'text' }
            ]
        },
        user_id: null,
        users_types: [],
        userErrors: [],
        passwordErrors: [],
        avatar: '',
        bread: {
            'Lista de Usuarios': { title: 'Usuarios del CMS', sections: ['Usuarios'],  icon: 'fas fa-users' },
            'Crear Usuario': { title: 'Crear Usuarios del CMS', sections: ['Usuarios', 'Crear'], icon: 'fas fa-users' },
            'Editar Usuario': { title: 'Actualizar Usuarios del CMS', sections: ['Usuarios', 'Actualizar'], icon: 'fas fa-users' }
        }
    },
    mutations: {
        setUpdateOn: (state, payload) => { state.updateOn = payload },
        setUsers: (state, payload) => { state.users = payload },
        setPagination: (state, payload) => { 
            state.pagination = payload
            delete state.pagination.data
        },
        setFiltros: (state, payload) => {
            state.filtro.selected = payload.selected
            state.filtro.busqueda = payload.busqueda
        },
        setUsersTypes: (state, payload) => { state.users_types = payload },
        setUserErrors: (state, payload) => { state.userErrors = payload },
        setPasswordErrors: (state, payload) => { state.passwordErrors = payload },
        setUserID: (state, payload) => { state.user_id = payload },
        setAvatar: (state, payload) => { state.avatar = payload },
        setUserBlock: (state, payload) => {
            let index = findIndex(state.users, (o) => { return o.id == payload.id })
            state.users[index].blocked_at = payload.blocked_at
        },
        setDeletedUsuario: (state, payload) => {
            let index = findIndex(state.users, (o) => { return o.id == payload.id })
            state.users.splice(index, 1)
        }
    },
    getters: {
        getBread: state => { return state.bread },
        getUpdateOn: state => { return state.updateOn },
        hasDataUsers: state => {
            if (state.users.length == 0) { return false }
            else { return true }
        },
        getUsers: state => { return state.users },
        getPagination: state => { return state.pagination },
        getFiltros: state => { return state.filtro },
        getUsersTypes: state => { return state.users_types },
        getUserErrors: state => { return state.userErrors },
        getPasswordErrors: state => { return state.passwordErrors },
        getUserID: state => { return state.user_id },
        getAvatar: state => { return state.avatar },
        hasAvatar: state => { if (state.avatar == '') { return false } else { return true } }
    },
    actions: {
        getUsuarios: ({ commit }, params) => {
            var route = (params.url === "" || params.url === null) ? '/users/get' : params.url
            api.get(route, { params: { tipo: params.filtro.selected, busqueda: params.filtro.busqueda } }).then((response) => {
                commit('setUsers', response.data.data)
                commit('setPagination', response.data)
            }).catch((error) => {
                console.log(error)
            })
        },
        getUsersTypes: ({ commit }) => {
            api.get('users/types').then((response) => {
                commit('setUsersTypes', response.data)
            }).catch((error) => {
                console.log(error)
            })
        },
        createUsuario: ({ commit }, userData) => {
            commit('setUserErrors', [])
            return api.post('users/create', userData).then((response) => {
                commit('setAvatar', response.data.avatar)
                return response.data
            }).catch((error) => {
                console.log(error.message)
                commit('setUserErrors', error.response.data.errors)
                throw error
            })
        },
        getUsuario: ({ commit }, id) => {
            return api.get('users/get/' + id).then((response) => {
                commit('setAvatar', response.data.avatar)
                return response.data
            }).catch((error) => {
                console.log(error)
                throw error
            })
        },
        updateUsuario: ({ commit }, userData) => {
            commit('setUserErrors', [])
            return api.post('users/update', userData).then((response) => {
                return response.data
            }).catch((error) => {
                console.log(error.message)
                commit('setUserErrors', error.response.data.errors)
                throw error
            })
        },
        updatePassword: ({ commit }, passwordData) => {
            commit('setPasswordErrors', [])
            return api.post('users/password', passwordData).then((response) => {
                return response
            }).catch((error) => {
                console.log(error.message)
                commit('setPasswordErrors', error.response.data.errors)
                throw error
            })
        },
        blockUsuario: ({ commit }, idUser) => {
            return api.post('users/block', { id: idUser }).then((response) => {
                commit('setUserBlock', response.data)
                return response
            }).catch((error) => {
                console.log(error.message)
                throw error
            })
        },
        deleteUsuario: ({ commit }, idUser) => {
            return api.delete('users/delete', { params: { id:idUser } }).then((response) => {
                commit('setDeletedUsuario', response.data)
                return response
            }).catch((error) => {
                console.log(error.message)
                throw error
            })
        }
    }
}
