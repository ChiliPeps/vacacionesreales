import api from '../../api'
import { findIndex } from 'lodash'

export default {
    namespaced: true,
    state: {
        updateOn: false,
        category: {
            id: '',
            nombre: ''
        },
        categories: [],
        pagination: null,
        filtro: {
            selected: 'nombre',
            busqueda: '',
            opciones: [
                { text: 'Nombre', value: 'nombre', type: 'text' }
            ]
        },
        categoryErrors: [],
        bread: {
            'Lista de Categorías': { title: 'Categorías del Blog', sections: ['Categorías'],  icon: 'fas fa-boxes' },
            'Crear Categoría': { title: 'Crear Categoría para el Blog', sections: ['Categorías', 'Crear'], icon: 'fas fa-boxes' },
            'Editar Categoría': { title: 'Actualizar Categoría para el Blog', sections: ['Categorías', 'Actualizar'], icon: 'fas fa-boxes' }
        }
    },
    mutations: {
        setUpdateOn: (state, payload) => { state.updateOn = payload },
        setCategories: (state, payload) => { state.categories = payload },
        setPagination: (state, payload) => { 
            state.pagination = payload
            delete state.pagination.data
        },
        setFiltros: (state, payload) => {
            state.filtro.selected = payload.selected
            state.filtro.busqueda = payload.busqueda
        },
        setCategoryAll: (state, payload) => {
            state.category = {
                id: payload.id,
                nombre: payload.nombre
            }
        },
        setCategory: (state, payload) => { state.category = payload },
        setCategoryId: (state, payload) => { state.category.id = payload },
        setCategoryNombre: (state, payload) => { state.category.nombre = payload },
        setCategoryErrors: (state, payload) => { state.categoryErrors = payload },
        cleanCategory: (state) => {
            state.category = {
                id: '',
                nombre: ''
            }
        },
        setDeletedCategoria: (state, payload) => {
            let index = findIndex(state.categories, (o) => { return o.id == payload.id })
            state.categories.splice(index, 1)
        }
    },
    getters: {
        getBread: state => { return state.bread },
        getUpdateOn: state => { return state.updateOn },
        hasDataCategories: state => {
            if (state.categories.length == 0) { return false }
            else { return true }
        },
        getCategories: state => { return state.categories },
        getCategory: state => { return state.category },
        getPagination: state => { return state.pagination },
        getFiltros: state => { return state.filtro },
        getCategoryId: state => { return state.category.id },
        getCategoryNombre: state => { return state.category.nombre },
        getCategoryErrors: state => { return state.categoryErrors }
    },
    actions: {
        getCategorias: ({ commit }, params) => {
            var route = (params.url === "" || params.url === null) ? '/categorias/get' : params.url
            api.get(route, { params: { tipo: params.filtro.selected, busqueda: params.filtro.busqueda } }).then((response) => {
                commit('setCategories', response.data.data)
                commit('setPagination', response.data)
            }).catch((error) => {
                console.log(error)
            })
        },
        createCategoria: ({ commit, getters }) => {
            commit('setCategoryErrors', [])
            return api.post('categorias/create', { nombre: getters.getCategory.nombre }).then((response) => {
                return response.data
            }).catch((error) => {
                console.log(error.message)
                commit('setCategoryErrors', error.response.data.errors)
                throw error
            })
        },
        getCategoria: ({ commit }, id) => {
            return api.get('categorias/get/' + id).then((response) => {
                commit('setCategoryAll', response.data)
                return response.data
            }).catch((error) => {
                console.log(error)
                throw error
            })
        },
        updateCategoria: ({ commit, getters }) => {
            commit('setCategoryErrors', [])
            return api.post('categorias/update', getters.getCategory).then((response) => {
                return response.data
            }).catch((error) => {
                console.log(error.message)
                commit('setCategoryErrors', error.response.data.errors)
                throw error
            })
        },
        deleteCategoria: ({ commit }, idCategoria) => {
            return api.delete('categorias/delete', { params: { id:idCategoria } }).then((response) => {
                commit('setDeletedCategoria', response.data)
                return response
            }).catch((error) => {
                console.log(error.message)
                throw error
            })
        }
    }
}
