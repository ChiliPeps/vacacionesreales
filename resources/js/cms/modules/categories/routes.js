// Componentes
import Base from './base'
import Index from './index'
import Inputs from './inputs'

export default [
    {
        path: '/categorias',
        component: Base,
        children: [
            {
                name: 'Lista de Categorías',
                path: 'index',
                component: Index
            },
            {
                name: 'Crear Categoría',
                path: 'create',
                component: Inputs,
            },
            {
                name: 'Editar Categoría',
                path: 'edit/:id',
                component: Inputs
            }
        ]
    }
]