// Componentes
import Base from './base'
import resultados from './resultados'

export default [
    {
        path: '/busqueda',
        component: Base,
        children: [
            {
                name: 'Resultados de Busqueda',
                path: 'resultados',
                component: resultados
            }
        ]
    }
]