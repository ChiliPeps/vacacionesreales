import api from '../../api'
import { filter } from 'lodash'

export default {
    namespaced: true,
    state: {
        search_text: '',
        resultados: [],
        bread: {
            'Resultados de Busqueda': { title: 'Resultados de busqueda', sections: ['Busqueda'],  icon: 'fas fa-search' }
        }
    },
    mutations: {
        setResultados: (state, payload) => { state.resultados = payload },
        setSearchText: (state, payload) => { state.search_text = payload }
    },
    getters: {
        getBread: state => { return state.bread },
        getResultados: state => { return state.resultados },
        getSearchText: state => { return state.search_text },
        getCategories: state => { return filter(state.resultados, (o) => { return o.busqueda_type == 'categoria' }) },
        getCMSUsers: state => { return filter(state.resultados, (o) => { return o.busqueda_type == 'cms_user' }) },
        getBlogPosts: state => { return filter(state.resultados, (o) => { return o.busqueda_type == 'post' }) }
    },
    actions: {
        getBusqueda: ({ commit, getters }) => {
            api.get('/busqueda/get', { params: { busqueda: getters.search_text } }).then((response) => {
                commit('setResultados', response.data)
            }).catch((error) => {
                console.log(error)
            })
        }
    }
}
