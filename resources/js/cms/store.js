import Vue from 'vue'
import Vuex from 'vuex'

// Modulos
import components from './components/store'
import home from './modules/home/store'
import search from './modules/search/store'
import users from './modules/users/store'
import categories from './modules/categories/store'
import blog from './modules/blog/store'
import gallery from './modules/gallery/store'
import configuration from './modules/configuration/store'

Vue.use(Vuex)

const store = new Vuex.Store({
    modules: {
        components,
        home,
        search,
        users,
        categories,
        blog,
        gallery,
        configuration
    }
})

export default store