import axios from 'axios'
import config from './config'

axios.defaults.baseURL = config.publicUrl + 'admin'
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
if (config.token) {
	axios.defaults.headers.common['X-CSRF-TOKEN'] = config.token
} else {
	console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token')
}

axios.interceptors.response.use(function (response) {
    return response
}, function (error) {
    if (error.response.data.server_unauthorized) {
        console.error("La Sesión ha expirado. Refresque la página.")
    }
    return Promise.reject(error)
})

export default axios