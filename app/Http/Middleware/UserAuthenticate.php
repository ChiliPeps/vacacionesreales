<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class UserAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // Not Logged Users
        if(!Auth::guard()->check()) {
            if($request->ajax()) {
                return response()->json(['server_unauthorized' => true], 401);
            } else { return redirect('/login'); }
        }

        // Blocked Users
        if(Auth::guard()->user()->blocked_at != null) {
            if($request->ajax()) {
                return response()->json(['server_unauthorized' => true, 'server_error_reason' => 'blocked_user'], 401);
            } else { return redirect('/login'); }
        }

        return $next($request);
    }
}
