<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CMS\Categoria;
use App\Models\CMS\Blog;
use App\CMSUser;
use App\Helpers\CMSHelpers;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class TestController extends Controller
{
    public function test()
    {
        $busqueda = '';

        // Blog
        $blog = Blog::with(['categoria', 'cms_user'])->where('titulo', 'LIKE', '%'.$busqueda.'%')
        ->orderBy('created_at', 'desc')->take(30)->get();
        foreach ($blog as $post) { $post->busqueda_type = 'blog'; }
        
        // Categoría
        $categorias = Categoria::where('nombre', 'LIKE', '%'.$busqueda.'%')
        ->orderBy('created_at', 'desc')->take(30)->get();
        foreach ($categorias as $categoria) { $categoria->busqueda_type = 'categoria'; }

        // CMS User
        $cms_users = CMSUser::where('nombre', 'LIKE', '%'.$busqueda.'%')
        ->orderBy('created_at', 'desc')->take(30)->get();
        foreach ($cms_users as $user) { $user->busqueda_type = 'cms_user'; }

        $results = new Collection();
        $results = $results->merge($blog);
        $results = $results->merge($categorias);
        $results = $results->merge($cms_users);

        return response()->json($results);
    }
}
