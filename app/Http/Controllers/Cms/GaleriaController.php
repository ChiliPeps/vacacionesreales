<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CMS\Galeria\Imagen;
use App\Models\CMS\Galeria;
use Auth;
use Image;
use Storage;

class GaleriaController extends Controller
{
    public function __construct() 
    {
        $this->middleware('CMSAuthenticate');
    }

    public function getGaleria() {
        $galeria = Galeria::with(['cms_user'])->orderBy('created_at', 'desc')->paginate(30);
        return response()->json($galeria);
    }

    public function uploadImagenes(Imagen $request)
    {
        $image = $request->file('file');
        $file_name = $image->getClientOriginalName();
        $directory = 'cms/galeria';
        $directoryThumbs = 'cms/galeria/thumbs';
        $path = $image->storeAs($directory, $file_name, 'public');
        $pathThumb = $image->storeAs($directoryThumbs, $file_name, 'public');

        // Make Thumb
        $thumbnailpath = public_path('storage/cms/galeria/thumbs/'.$file_name);
        $img = Image::make($thumbnailpath)->resize(300, 300, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->save($thumbnailpath);

        // Create Record
        $imagen = Galeria::create([
            'titulo'       => $file_name,
            'descripcion'  => 'null',
            'imagen'       => $path,
            'thumb'        => $pathThumb,
            'id_cms_user'  => Auth::guard('cms')->user()->id
        ]);

        return response()->json($imagen);
    }

    public function deleteImagen(Request $request)
    {
        $imagen = Galeria::findOrFail($request->id);
        Storage::disk('public')->delete('cms/galeria/'.$imagen->imagen);
        Storage::disk('public')->delete('cms/galeria/thumbs/'.$imagen->thumb);
        $imagen->delete();
        return response()->json($imagen);
    }
}
