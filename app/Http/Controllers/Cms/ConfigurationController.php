<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CMS\Configuration;
use App\Http\Requests\CMS\Configuration\Contacto;
use App\Http\Requests\CMS\Configuration\Colores;
use App\Http\Requests\CMS\Configuration\Efectos;
use App\Http\Requests\CMS\Configuration\LoginBack;
use App\Http\Requests\CMS\Configuration\SiteUp;
use App\Http\Requests\CMS\Configuration\Buscador;
use App\Http\Requests\CMS\Configuration\Shortcuts;
use App\Http\Requests\CMS\Configuration\Logo;

class ConfigurationController extends Controller
{
    public function __construct() 
    {
        $this->middleware('CMSAuthenticate');
    }

    public function getConfiguration() {
        return response()->json($this->getOptionsArray());
    }

    protected function getOptionsArray() {
        $configuration = Configuration::first();
        return json_decode($configuration->options);
    }

    protected function saveOptionsData($options) {
        $configuration = Configuration::first();
        $configuration->options = json_encode($options);
        $configuration->save();
        return $configuration;
    }

    public function setContacto(Contacto $request)
    {
        $options = $this->getOptionsArray();
        $options->correo_contacto = $request->correo_contacto;
        $configuration = $this->saveOptionsData($options);
        return response()->json($configuration);
    }

    public function setColores(Colores $request)
    {
        $options = $this->getOptionsArray();
        $options->template_layout_options = $request->template_layout_options;
        $options->navbar_variants = $request->navbar_variants;
        $options->accent_color_variants = $request->accent_color_variants;
        $options->sidebar_variants = $request->sidebar_variants;
        $options->brand_logo_variants = $request->brand_logo_variants;
        $configuration = $this->saveOptionsData($options);
        return response()->json($configuration);
    }

    public function setLoginback(LoginBack $request)
    {
        $options = $this->getOptionsArray();
        $file_name = "backlogin.".$request->file('image')->getClientOriginalExtension();
        $path = $request->file('image')->storeAs('cms/configuration', $file_name, 'public');
        $options->login_background_url = $path;
        $configuration = $this->saveOptionsData($options);
        return response()->json($path);
    }

    public function setEfectos(Efectos $request)
    {
        $options = $this->getOptionsArray();
        $options->principal_transition = $request->principal_transition;
        $options->secondary_transition = $request->secondary_transition;
        $configuration = $this->saveOptionsData($options);
        return response()->json($configuration);
    }

    public function setSiteUp(SiteUp $request)
    {
        $options = $this->getOptionsArray();
        $options->front_site_up = $request->front_site_up;
        $configuration = $this->saveOptionsData($options);
        return response()->json($configuration);
    }

    public function setBuscadorHeader(Buscador $request)
    {
        $options = $this->getOptionsArray();
        $options->buscador_header = $request->buscador_header;
        $configuration = $this->saveOptionsData($options);
        return response()->json($configuration);
    }

    public function setShortcuts(Shortcuts $request)
    {
        $options = $this->getOptionsArray();
        $options->shortcuts = $request->shortcuts;
        $configuration = $this->saveOptionsData($options);
        return response()->json($configuration);
    }

    public function setLogoCMS(Logo $request)
    {
        $options = $this->getOptionsArray();
        $file_name = "logo.".$request->file('image')->getClientOriginalExtension();
        $path = $request->file('image')->storeAs('cms/configuration', $file_name, 'public');
        $options->login_background_url = $path;
        $configuration = $this->saveOptionsData($options);
        return response()->json($path);
    }
}
