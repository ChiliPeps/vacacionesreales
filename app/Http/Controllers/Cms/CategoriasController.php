<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CMS\Categoria\Create;
use App\Http\Requests\CMS\Categoria\Update;
use App\Models\CMS\Categoria;

class CategoriasController extends Controller
{
    public function __construct() 
    {
        $this->middleware('CMSAuthenticate');
    }

    public function getCategorias(Request $request) {
        $tipo = $request->tipo;
        $busqueda = $request->busqueda;

        $results = Categoria::where($tipo, 'LIKE', '%'.$busqueda.'%')
        ->orderBy('created_at', 'desc')->paginate(20);
        
        return response()->json($results);
    }

    public function getCategoria(Request $request)
    {
        $categoria = Categoria::where('id', $request->id)->first();
        return response()->json($categoria);
    }

    public function createCategoria(Create $request)
    {
        $categoria = Categoria::create([
            'nombre' => $request->nombre
        ]);
        return response()->json($categoria);
    }

    public function updateCategoria(Update $request)
    {
        $categoria = Categoria::findOrFail($request->id);
        $categoria->fill([
            'nombre' => $request->nombre
        ]);
        $categoria->save();
        return response()->json($categoria);
    }

    public function deleteCategoria(Request $request)
    {
        $categoria = Categoria::findOrFail($request->id);
        $categoria->delete();
        return response()->json($categoria);
    }

    public function getListCategorias()
    {
        $categorias = Categoria::orderBy('nombre', 'asc')->get();
        return response()->json($categorias);
    }
}
