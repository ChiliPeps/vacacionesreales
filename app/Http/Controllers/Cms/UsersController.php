<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CMS\Users\Create;
use App\Http\Requests\CMS\Users\Update;
use App\Http\Requests\CMS\Users\Password;
use App\Http\Requests\CMS\Users\Avatar;
use App\CMSUser;
use Storage;
use Auth;
use Carbon\Carbon;

class UsersController extends Controller
{
    protected $defaultAvatar = "cms/avatars/pablo.png";

    public function __construct() 
    {
        $this->middleware('CMSAuthenticate');
    }

    public function getUsers(Request $request) {
        $tipo = $request->tipo;
        $busqueda = $request->busqueda;

        $results = CMSUser::where($tipo, 'LIKE', '%'.$busqueda.'%')
        ->orderBy('created_at', 'desc')->paginate(20);

        return response()->json($results);
    }

    public function getUser(Request $request)
    {
        $user = CMSUser::where('id', $request->id)->first();
        return response()->json($user);
    }

    public function createUser(Create $request)
    {
        $user = CMSUser::create([
            'nombre'   => $request->nombre,
            'apellido' => $request->apellido,
            'avatar'   => $this->defaultAvatar,
            'email'    => $request->email,
            'password' => bcrypt('123456'),
            'type'     => $request->type
        ]);
        return response()->json($user);
    }

    public function updateUser(Update $request)
    {
        $user = CMSUser::findOrFail($request->id);
        $user->fill([
            'nombre'   => $request->nombre,
            'apellido' => $request->apellido,
            'type'     => $request->type
        ]);
        $user->save();
        return response()->json($user);
    }

    public function updatePassword(Password $request)
    {   
        $user = CMSUser::findOrFail($request->id);
        $user->password = bcrypt($request->password);
        $user->save();
        return response()->json($user);
    }

    public function updateAvatar(Avatar $request)
    {
        $user = CMSUser::findOrFail($request->id);
        $path = $request->file('avatar')->store('cms/avatars', 'public');

        // Borrar Antiguo (No el Default)
        if ($user->avatar != $this->defaultAvatar) {
            Storage::disk('public')->delete($user->avatar);
        }

        $user->avatar = $path;
        $user->save();
        return response()->json($user);
    }

    public function defaultAvatar(Request $request)
    {
        $user = CMSUser::findOrFail($request->id);
        $user->avatar = $this->defaultAvatar;
        $user->save();
        return response()->json($user);
    }

    public function deleteUser(Request $request)
    {
        $user = CMSUser::findOrFail($request->id);

        // Same User Block Validation
        if(Auth::guard('cms')->user()->id == $user->id) {
            return response()->json("Error you are the same.");
        }

        // Borrar Antiguo (No el Default)
        if ($user->avatar != $this->defaultAvatar) {
            Storage::disk('public')->delete($user->avatar);
        }
        
        $user->delete();
        return response()->json($user);
    }

    public function toggleBlock(Request $request)
    {
        $user = CMSUser::findOrFail($request->id);

        // Same User Block Validation
        if(Auth::guard('cms')->user()->id == $user->id) {
            return response()->json("Error you are the same.");
        }

        if ($user->blocked_at == null) {
            $user->blocked_at = Carbon::now();
        } else { $user->blocked_at = null; }
        $user->save();
        return response()->json($user);
    }

    public function getUsersTypes()
    {
        $types = config('cms.user_types');
        return response()->json($types);
    }
}
