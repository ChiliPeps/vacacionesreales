<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class AdminController extends Controller
{
    public function __construct() 
    {
        $this->middleware('CMSAuthenticate');
    }

    public function home()
    {
        return view('cms.app');
    }

    public function getUserInfo() {
        $user = Auth::guard('cms')->user();
        return response()->json($user);
    }
}
