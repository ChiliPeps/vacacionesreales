<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CMS\Blog\Create;
use App\Http\Requests\CMS\Blog\Update;
use App\Http\Requests\CMS\Blog\Imagen;
use App\Http\Requests\CMS\Blog\TinyImage;
use App\Http\Requests\CMS\Blog\Categoria as CategoriaCreate;
use App\Models\CMS\Blog;
use App\Models\CMS\Categoria;
use Auth;
use Image;
use Carbon\Carbon;

class BlogController extends Controller
{
    public function __construct() 
    {
        $this->middleware('CMSAuthenticate');
    }

    public function getPosts(Request $request) {
        $withs = ['categoria', 'cms_user'];
        $tipo = $request->tipo;
        $busqueda = $request->busqueda;

        if ($tipo == 'fecha') {
            $from = Carbon::createFromFormat('Y-m-d', $busqueda[0])->toDateString();
            $to = Carbon::createFromFormat('Y-m-d', $busqueda[1])->toDateString();
            $results = Blog::with($withs)->whereBetween('fecha', [$from, $to])
            ->orderBy('created_at', 'desc')->paginate(20);
        } elseif ($tipo == 'categoria') {
            $ids_categorias = Categoria::where('nombre', 'LIKE', '%'.$busqueda.'%')->pluck('id')->toArray();
            $results = Blog::with($withs)->whereIn('id_categoria', $ids_categorias)
            ->orderBy('created_at', 'desc')->paginate(20);
        } else {
            $results = Blog::with($withs)->where($tipo, 'LIKE', '%'.$busqueda.'%')
            ->orderBy('created_at', 'desc')->paginate(20);
        }
       
        return response()->json($results);
    }

    public function getPost(Request $request)
    {
        $post = Blog::where('id', $request->id)->first();
        return response()->json($post);
    }

    public function createPost(Create $request)
    {
        $post = Blog::create([
            'titulo'       => $request->titulo,
            'fecha'        => $request->fecha,
            'descripcion'  => $request->descripcion,
            'contenido'    => $request->contenido,
            'autor'        => $request->autor,
            'slugurl'      => $request->slugurl,
            'imagen'       => $request->imagen,
            'thumb'        => $request->thumb,
            'autor_foto'   => $request->autor_foto,
            'publish'      => $request->publish,
            'id_categoria' => $request->id_categoria,
            'id_cms_user'  => Auth::guard('cms')->user()->id
        ]);
        return response()->json($post);
    }

    public function updatePost(Update $request)
    {
        $post = Blog::findOrFail($request->id);
        $post->fill([
            'titulo'       => $request->titulo,
            'fecha'        => $request->fecha,
            'descripcion'  => $request->descripcion,
            'contenido'    => $request->contenido,
            'autor'        => $request->autor,
            'slugurl'      => $request->slugurl,
            'imagen'       => $request->imagen,
            'thumb'        => $request->thumb,
            'autor_foto'   => $request->autor_foto,
            'publish'      => $request->publish,
            'id_categoria' => $request->id_categoria
        ]);
        $post->save();
        return response()->json($post);
    }

    public function uploadImage(Imagen $request)
    {
        $image = $request->file('imagenblog');
        $file_name = time().'.'.$image->extension();
        $directory = 'cms/blog';
        $directoryThumbs = 'cms/blog/thumbs';
        $path = $image->storeAs($directory, $file_name, 'public');
        $pathThumb = $image->storeAs($directoryThumbs, $file_name, 'public');

        // Make Thumb
        $thumbnailpath = public_path('storage/cms/blog/thumbs/'.$file_name);
        $img = Image::make($thumbnailpath)->resize(300, 300, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->save($thumbnailpath);

        // Paths Array
        $paths = [
            'path' => $path,
            'path_thumb' => $pathThumb
        ];

        return response()->json($paths);
    }

    public function deletePost(Request $request)
    {
        $post = Blog::findOrFail($request->id);
        $post->delete();
        return response()->json($post);
    }

    public function togglePublish(Request $request)
    {
        $post = Blog::findOrFail($request->id);
        if ($post->publish == 0) {
            $post->publish = 1;
        } else { $post->publish = 0; }
        $post->save();
        return response()->json($post);
    }

    public function uploadTinyImage(TinyImage $request)
    {
        $file_name = time().'.'.$request->file('imagen')->getClientOriginalExtension();
        $path = $request->file('imagen')->storeAs('cms/tinyimages', $file_name, 'public');
        return response()->json($path);
    }

    public function createCategoria(CategoriaCreate $request) 
    {
        $categoria = Categoria::create([
            'nombre' => $request->nombre
        ]);
        return response()->json($categoria); 
    }
}
