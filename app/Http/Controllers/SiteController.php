<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function inicio()
    {
        return view('pages.inicio');
    }

    public function hoteles()
    {
        return view('pages.hoteles');
    }

    public function promociones()
    {
        return view('pages.promociones');
    }

	public function contacto()
    {
        return view('pages.contacto');
    }

    public function galeria()
    {
    	return view('pages.galeria');
    }

    public function destino()
    {
        return view('pages.destino');
    }

    public function blog()
    {
        return view('pages.blog');
    }

    public function entrada()
    {
        return view('pages.entrada');
    }



}