<?php

namespace App\Http\Requests\CMS\Blog;

use Illuminate\Foundation\Http\FormRequest;

class Create extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo'   => 'required',
            'fecha'    => 'required',
            'autor'    => 'required',
            'slugurl'  => 'required',
            'publish'  => 'required',
            'id_categoria' => 'required',
            'descripcion'  => 'required',
            'contenido'    => 'required',
            'imagen'       => 'required'
        ];
    }

    public function messages()
    {
        return [
            'titulo.required' => 'El título del post es requerido',
            'fecha.required' => 'El apellido es requerido',
            'autor.required' => 'El autor del post es requerido',
            'slugurl.required' => 'El campo slugurl es requerido',
            'id_categoria.required' => 'La categoría es requerida',
            'descripcion.required' => 'La descripción es requerida',
            'contenido.required' => 'El contenido es requerido',
            'imagen.required' => 'La imagen es requerida'
        ];
    }
}
