<?php

namespace App\Http\Requests\CMS\Blog;

use Illuminate\Foundation\Http\FormRequest;

class TinyImage extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'imagen' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ];
    }

    public function messages()
    {
        return [
            'imagen.required' => 'La imagen es requerida',
            'imagen.image' => 'El archivo debe ser un archivo de imagen',
            'imagen.max' => 'La imagen no debe ser mayor a 2MB'
        ];
    }
}
