<?php

namespace App\Http\Requests\CMS\Users;

use Illuminate\Foundation\Http\FormRequest;

class Password extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'        => 'required',
            'password'  => 'required|confirmed'
        ];
    }

    public function messages()
    {
        return [
            'id.required' => 'El id del usuario es requerido',
            'password.required' => 'La contraseña es requerido',
            'password.confirmed' => 'Es requerido confirmar la contraseña'
        ];
    }
}
