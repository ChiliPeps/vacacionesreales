<?php

namespace App\Http\Requests\CMS\Users;

use Illuminate\Foundation\Http\FormRequest;

class Create extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'   => 'required',
            'apellido' => 'required',
            'email'    => 'email|required|unique:cms_users',
            'type'     => 'required'
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El nombre es requerido',
            'apellido.required' => 'El apellido es requerido',
            'email.required' => 'El email es requerido',
            'email.email' => 'El email no tiene el formato correcto',
            'email.unique' => 'Ya existe un email con el mismo nombre registrado',
            'type.required' => 'El tipo de usuario es requerido'
        ];
    }
}
