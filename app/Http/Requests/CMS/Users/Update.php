<?php

namespace App\Http\Requests\CMS\Users;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'       => 'required',
            'nombre'   => 'required',
            'apellido' => 'required',
            'type'     => 'required'
        ];
    }

    public function messages()
    {
        return [
            'id.required' => 'El id del usuario es requerido',
            'nombre.required' => 'El nombre es requerido',
            'apellido.required' => 'El apellido es requerido',
            'type.required' => 'El tipo de usuario es requerido'
        ];
    }
}
