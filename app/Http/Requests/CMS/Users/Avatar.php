<?php

namespace App\Http\Requests\CMS\Users;

use Illuminate\Foundation\Http\FormRequest;

class Avatar extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ];
    }

    public function messages()
    {
        return [
            'avatar.required' => 'El avatar es requerido',
            'avatar.image' => 'El avatar debe ser un archivo de imagen',
            'avatar.max' => 'La imagen no debe ser mayor a 2MB'
        ];
    }
}
