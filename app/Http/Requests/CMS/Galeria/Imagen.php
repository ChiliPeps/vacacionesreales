<?php

namespace App\Http\Requests\CMS\Galeria;

use Illuminate\Foundation\Http\FormRequest;

class Imagen extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ];
    }

    public function messages()
    {
        return [
            'file.required' => 'La imagen es requerida',
            'file.image' => 'Debe ser un archivo de imagen',
            'file.max' => 'La imagen no debe ser mayor a 2MB'
        ];
    }
}
