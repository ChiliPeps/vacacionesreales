<?php

namespace App\Http\Requests\CMS\Configuration;

use Illuminate\Foundation\Http\FormRequest;

class Contacto extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'correo_contacto' => 'required|email'
        ];
    }

    public function messages()
    {
        return [
            'correo_contacto.required' => 'El correo de contacto es requerido',
            'correo_contacto.email' => 'El correo de contacto no tiene el formato correcto'
        ];
    }
}
