<?php

namespace App\Http\Requests\CMS\Configuration;

use Illuminate\Foundation\Http\FormRequest;

class LoginBack extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ];
    }

    public function messages()
    {
        return [
            'image.required' => 'La imagen del fondo del login es requerida',
            'image.image' => 'La imagen del fondo del login debe ser un archivo de imagen',
            'image.max' => 'La imagen no debe ser mayor a 2MB'
        ];
    }
}
