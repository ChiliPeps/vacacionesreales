<?php

namespace App\Http\Requests\CMS\Configuration;

use Illuminate\Foundation\Http\FormRequest;

class Shortcuts extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'shortcuts' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'shortcuts.required' => 'Los atajos son requeridos'
        ];
    }
}