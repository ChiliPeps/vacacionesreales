<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
    protected $table = 'cms_configuration';
    protected $fillable = ['options'];
}