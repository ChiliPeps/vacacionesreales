<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blog extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = 'blog';
    protected $fillable = [
        'titulo', 'fecha', 'id_categoria', 'descripcion', 
        'contenido', 'autor', 'imagen', 'thumb', 'autor_foto', 
        'visitas', 'slugurl', 'publish', 'id_cms_user'
    ];

    public function categoria()
    {
        return $this->belongsTo('App\Models\CMS\Categoria', 'id_categoria');
    }

    public function cms_user()
    {
        return $this->belongsTo('App\CMSUser', 'id_cms_user');
    }
}
