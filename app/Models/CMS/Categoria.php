<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Categoria extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = 'categorias';
    protected $fillable = ['nombre'];

    public function posts()
    {
        return $this->hasMany('App\Models\CMS\Blog', 'id_categoria');
    }
}
