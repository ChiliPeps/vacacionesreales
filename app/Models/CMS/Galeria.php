<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;

class Galeria extends Model
{
    protected $table = 'galeria';
    protected $fillable = ['titulo', 'descripcion', 'imagen', 'thumb', 'id_cms_user'];

    public function cms_user()
    {
        return $this->belongsTo('App\CMSUser', 'id_cms_user');
    }
}
