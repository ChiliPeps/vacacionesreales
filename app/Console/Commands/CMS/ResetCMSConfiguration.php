<?php

namespace App\Console\Commands\CMS;

use Illuminate\Console\Command;
use App\Helpers\CMSHelpers;

class ResetCMSConfiguration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms:reset:configuration';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reiniciar configuración del CMS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('*********************************');
        $this->info('Reiniciar configuración del CMS');
        $this->info('*********************************');

        CMSHelpers::resetCMSConfig();

        $this->info('Configuración del CMS Reiniciada!');
        $this->info('Así no más quedó!');
    }
}
