<?php

namespace App\Console\Commands\CMS;

use Illuminate\Console\Command;
use Hash;
use App\CMSUser;
use App\Models\CMS\Configuration;
use App\Helpers\CMSHelpers;

class AddFakeAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms:addfakeadmin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Agregar Admin Admin al CMS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('*********************************');
        $this->info('Agregando a Admin Admin al CMS');
        $this->info('*********************************');

        // Check if Exist!
        $user = CMSUser::where('email', 'admin@admin.com')->first();
        if ($user != null) {
            $this->info('El usuario ya existe!');
        } else {
            CMSUser::create([
                'nombre'   => 'Admin',
                'apellido' => 'Admin',
                'email'    => 'admin@admin.com',
                'password' => Hash::make('admin'),
                'type'     => 'suadmin',
                'avatar'   => 'cms/avatars/pablo.png'
            ]);
            $this->checkFirstTime();
            $this->info('Usuario añadido!');
            $this->info('Que la fuerza te acompañe pariente!');
        }
    }

    protected function checkFirstTime()
    {
        $configuration = Configuration::first();
        if ($configuration == null) {
            CMSHelpers::resetCMSConfig();
        }
    }
}
