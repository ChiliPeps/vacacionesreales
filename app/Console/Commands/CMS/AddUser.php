<?php

namespace App\Console\Commands\CMS;

use Illuminate\Console\Command;
use Hash;
use App\CMSUser;
use App\Models\CMS\Configuration;
use Validator;
use App\Helpers\CMSHelpers;

class AddUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms:adduser';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Agregar nuevo usuario al CMS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('*********************************');
        $this->info('Agregar nuevo usuario al CMS');
        $this->info('*********************************');

        while (!$this->createUser()){}
        $this->info('Usuario añadido!');
        $this->info('Que la fuerza te acompañe pariente!');

    }

    protected function createUser()
    {
        $data = [];
        $data['nombre'] = $this->ask('¿Que nombre tiene el nuevo usuario?');
        $data['apellido'] = $this->ask('¿Que apellido tiene el nuevo usuario?');
        $data['email'] = $this->ask('Escribe su correo electronico:');
        $data['password'] = $this->secret('Escribe su contraseña:');
        $data['password_confirmation'] = $this->secret('Escribe su contraseña de nuevo');

        $validator = Validator::make($data, [
            'nombre'   => 'required',
            'apellido' => 'required',
            'email'    => 'required|email|unique:cms_users',
            'password' => 'required|confirmed',
        ]);

        if ($validator->fails()) {
            foreach($validator->errors()->all() as $error) {
                $this->error('Error: ' . $error);
            }
            $this->info('--------------------------------------');
            return false;
        }

        $data['type'] = $this->choice('Elegir un rol para el nuevo usuario:', config('cms.user_types'), 0);
        $data['password'] = Hash::make($data['password']);
        $data['avatar'] = "cms/avatars/pablo.png";

        CMSUser::create($data);
        $this->checkFirstTime();

        return true;
    }

    protected function checkFirstTime()
    {
        $configuration = Configuration::first();
        if ($configuration == null) {
            CMSHelpers::resetCMSConfig();
        }
    }
}
