<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notifications\UserResetPassword;
use Illuminate\Database\Eloquent\SoftDeletes;

class CMSUser extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = 'cms_users';

    protected $fillable = ['nombre', 'apellido', 'email', 'type', 'avatar', 'password'];

    protected $hidden = [
        'password', 'remember_token',
    ];

    //Blocked User
    public function isBlocked()
    {
        return $this->blocked_at != null;
    }
}
