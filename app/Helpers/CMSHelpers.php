<?php

namespace App\Helpers;

use Carbon\Carbon;
use App\CMSUser;
use App\Models\CMS\Configuration;
use Storage;
use Artisan;

class CMSHelpers {

    public static function cmsBodyClass()
    {
        $configuration = Configuration::first();
        $options = json_decode($configuration->options);
        $class = 'sidebar-mini ' . $options->template_layout_options; // template_layout_options options
        return $class;
    }

    public static function getCMSLoginBackground()
    {
        $configuration = Configuration::first();
        $options = json_decode($configuration->options);
        return $options->login_background_url;
    }

    public static function getConfigurationData()
    {
        return Configuration::first();
    }

    public static function getDate()
    {
        setlocale(LC_TIME, config('app.locale'));
        $fecha = Carbon::now()->formatLocalized('%A %d %B %Y');
        $hora  = Carbon::now()->toTimeString();
        return ucfirst($fecha) ." ". $hora;
    }

    public static function getUserCount()
    {
        return CMSUser::all()->count();
    }

    public static function shortDate($date)
    {
        setlocale(LC_TIME, config('app.locale'));
        $fecha = Carbon::createFromFormat('Y-m-d', substr($date, 0, 10))->formatLocalized('%d %B %Y');
        return ucfirst($fecha);
    }

    public static function getWeekDates()
    {
        $startOfWeek = Carbon::today()->startOfWeek();
        $endOfWeek = Carbon::today()->endOfWeek();

        $start_day = $startOfWeek->day;
        $start_month = $startOfWeek->month;

        $end_day = $endOfWeek->day;
        $end_month = $endOfWeek->month;

        $month_español = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", 
        "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

        return $month_español[$start_month - 1]." ".$start_day." - ".$month_español[$end_month - 1]." ".$end_day;
    }

    public static function checkInput($errors, $name)
    {
        if($errors->has($name)) {
            return "form-control is-invalid";
        } else {
            return "form-control";
        }
    }

    public static function resetCMSConfig()
    {
        $options = self::getInitialConfigurationTable();
        $configuration = Configuration::first();
        if ($configuration == null) {
            Configuration::create([
                'options' => json_encode($options)
            ]);
        } else {
            $configuration->options = json_encode($options);
            $configuration->save();
        }

        // Crear link para la carpeta storage
        if (!Storage::exists(public_path('storage'))) {
            Artisan::call('storage:link');
        } 

        $done = self::copyDefaultAvatar();
        $done = self::copyDefaultCMSLogo();
    }

    public static function copyDefaultAvatar()
    {
        Storage::disk('public')->put('cms/avatars/pablo.png', Storage::disk('real_public')->get('images/pablo.png'));
        return true;
    }

    public static function copyDefaultCMSLogo()
    {
        Storage::disk('public')->put('cms/configuration/komvac-icon-144x144.png', Storage::disk('real_public')->get('images/komvac-icon-144x144.png'));
        return true;
    }

    public static function getInitialConfigurationTable()
    {
        return [
            'template_layout_options' => 'null',
            'navbar_variants' => 'navbar-light navbar-white',
            'accent_color_variants' => 'null',
            'sidebar_variants' => 'sidebar-dark-primary',
            'brand_logo_variants' => 'null',
            'login_background_url' => 'null',
            'correo_contacto' => 'doradopaz@hotmail.com',
            'front_site_up' => '0',
            'buscador_header' => '1',
            'principal_transition' => 'fade',
            'secondary_transition' => 'fade',
            'shortcuts' => "[{\"id\":0,\"text\":\"Inicio\",\"route\":\"/\",\"icon\":\"fas fa-tachometer-alt\"},{\"id\":1,\"text\":\"Usuarios\",\"route\":\"/usuarios/index\",\"icon\":\"fas fa-users\"},{\"id\":2,\"text\":\"Configuración\",\"route\":\"/configuracion/edit\",\"icon\":\"fas fa-cogs\"}]",
            'logo_cms' => 'cms/configuration/komvac-icon-144x144.png'
        ];
    }
}



