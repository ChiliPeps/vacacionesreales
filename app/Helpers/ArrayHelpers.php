<?php

namespace App\Helpers;

class ArrayHelpers {

    public static function getOneElementArray($elements, $keyName) {
        $ids = array();
        for ($i = 0; $i < count($elements); $i++) { 
            $ids[] = $elements[$i][$keyName];
        }
        return $ids;
    }
    
    public static function findIndexByElementKey($elements, $keyName, $element) {
        $index = null;
        for ($i = 0; $i < count($elements); $i++) {
            if ($element == $elements[$i][$keyName]) {
                $index = $i; break;
            }
        } return $index;
    }
}

