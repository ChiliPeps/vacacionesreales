<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Blog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categorias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('blog', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->date('fecha');
            $table->text('descripcion');
            $table->text('contenido');
            $table->string('autor');
            $table->string('slugurl');
            $table->string('imagen');
            $table->string('thumb');
            $table->string('autor_foto');
            $table->integer('visitas')->default(0);
            $table->boolean('publish');
            
            $table->integer('id_categoria')->unsigned()->nullable();
            $table->foreign('id_categoria')->references('id')->on('categorias');

            $table->integer('id_cms_user')->unsigned()->nullable();
            $table->foreign('id_cms_user')->references('id')->on('cms_users');

            $table->timestamps();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog');
        Schema::dropIfExists('categorias');
    }
}
