<?php

use Illuminate\Database\Seeder;

class CMSCategoryAndBlog extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info(' Creando CMS Categorias & Blog...');

        // Generar CMS Categorias
        $cantidad_categorias = 15;
        $cantidad_posts = 10;
        $this->bar = $this->command->getOutput()->createProgressBar($cantidad_categorias);
        factory(App\Models\CMS\Categoria::class, $cantidad_categorias)->create()
        ->each(function ($categoria) use ($cantidad_posts) {
            
            // Generar Noticias de Esa Categoría
            $categoria->posts()->saveMany(factory(App\Models\CMS\Blog::class, $cantidad_posts)->make());

            $this->bar->advance();
        });
        $this->bar->finish();
        $this->command->info(' CMS Categorias & Blog creados...');
    }
}
