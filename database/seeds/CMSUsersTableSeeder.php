<?php

use Illuminate\Database\Seeder;

class CMSUsersTableSeeder extends Seeder
{
    protected $bar = null;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info(' Creando CMS Users...');

        // Generar CMS Users
        $cantidad_cms_users = 50;
        $this->bar = $this->command->getOutput()->createProgressBar($cantidad_cms_users);
        factory(App\CMSUser::class, $cantidad_cms_users)->create()
        ->each(function ($user) {
            
            $this->bar->advance();
        });
        $this->bar->finish();
        $this->command->info(' Users & Fotos creados...');
    }
}
