<?php

use Illuminate\Database\Seeder;

class CMSGallery extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info(' Creando CMS Galería...');

        // Generar CMS Galería
        $cantidad_imagenes = 50;
        $this->bar = $this->command->getOutput()->createProgressBar($cantidad_imagenes);
        factory(App\Models\CMS\Galeria::class, $cantidad_imagenes)->create()
        ->each(function ($galeria) {
            $this->bar->advance();
        });
        $this->bar->finish();
        $this->command->info(' CMS Galería creada...');
    }
}
