<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Models\CMS\Categoria::class, function (Faker $faker) {
    return [
        'nombre' => $faker->word
    ];
});
