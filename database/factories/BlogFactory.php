<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\CMSUser;

$factory->define(App\Models\CMS\Blog::class, function (Faker $faker) {

    // Fecha
    $datetime = $faker->dateTimeBetween($startDate = '-30 days', $endDate = 'now', $timezone = 'America/Mazatlan');
    $strtime = date_format($datetime, 'Y-m-d');
    $fecha = date('Y-m-d', strtotime($strtime));

    // CMS User
    $cms_user = CMSUser::inRandomOrder()->first();

    return [
        'titulo'       => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'fecha'        => $fecha,
        'descripcion'  => $faker->paragraph($nbSentences = 2, $variableNbSentences = true),
        'contenido'    => $faker->text($maxNbChars = 800),
        'autor'        => $faker->name,
        'slugurl'      => $faker->slug,
        'imagen'       => 'https://picsum.photos/640/480',
        'thumb'        => 'https://picsum.photos/320/240',
        'autor_foto'   => $faker->company,
        'publish'      => 1,
        'id_cms_user'  => $cms_user->id
    ];
});
