<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\CMSUser::class, function (Faker $faker) {
    return [
        'nombre' => $faker->name,
        'apellido' => $faker->lastname,
        'email' => $faker->unique()->safeEmail,
        'type' => $faker->randomElement(config('cms.user_types')),
        'avatar' => 'cms/avatars/pablo.png',
        'password' => bcrypt('123456'),
        'remember_token' => str_random(10),
        'blocked_at' => null
    ];
});
