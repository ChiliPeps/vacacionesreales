<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\CMSUser;

$factory->define(App\Models\CMS\Galeria::class, function (Faker $faker) {

    // CMS User
    $cms_user = CMSUser::inRandomOrder()->first();

    return [
        'titulo'       => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'descripcion'  => $faker->paragraph($nbSentences = 2, $variableNbSentences = true),
        'imagen'       => 'https://picsum.photos/640/480',
        'thumb'        => 'https://picsum.photos/320/240',
        'id_cms_user'  => $cms_user->id
    ];
});
