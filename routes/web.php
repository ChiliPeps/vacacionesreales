<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('master');
// });

Route::get('/',             ['as' => 'inicio',      'uses' => 'SiteController@inicio']);
Route::get('hoteles',       ['as' => 'hoteles',     'uses' => 'SiteController@hoteles']);
Route::get('promociones',   ['as' => 'promociones', 'uses' => 'SiteController@promociones']);
Route::get('contacto',      ['as' => 'contacto',    'uses' => 'SiteController@contacto']);
Route::get('galeria',       ['as' => 'galeria',     'uses' => 'SiteController@galeria']);
Route::get('destino/cabo',  ['as' => 'destino',     'uses' => 'SiteController@destino']);

// Blog
Route::get('blog',          ['as' => 'blog',        'uses' => 'SiteController@blog']);
Route::get('blog/noticia',  ['as' => 'entrada',     'uses' => 'SiteController@entrada']);



Route::get('/test', ['as' => 'test', 'uses' => 'TestController@test']);