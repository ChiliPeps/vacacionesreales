<?php

// Ajax Routes
// admin/categorias/get | admin.categorias.get | App\Http\Controllers\Cms\CategoriasController@getUserInfo | web,ajax,CMSAuthenticate
Route::group(['middleware' => ['ajax'], 'prefix' => 'categorias', 'as' => 'admin.'], function () {
    Route::get('get',       ['as' => 'categorias.get',      'uses' => 'Cms\CategoriasController@getCategorias']);
    Route::get('get/{id}',  ['as' => 'categorias.get.categoria', 'uses' => 'Cms\CategoriasController@getCategoria']);
    Route::post('create',   ['as' => 'categorias.create',   'uses' => 'Cms\CategoriasController@createCategoria']);
    Route::post('update',   ['as' => 'categorias.update',   'uses' => 'Cms\CategoriasController@updateCategoria']);
    Route::delete('delete', ['as' => 'categorias.delete',   'uses' => 'Cms\CategoriasController@deleteCategoria']);
    Route::get('list',      ['as' => 'categorias.list',     'uses' => 'Cms\CategoriasController@getListCategorias']);
});
