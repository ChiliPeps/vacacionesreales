<?php

// Ajax Routes
// admin/user/info | admin.user.get | App\Http\Controllers\Cms\UsersController@getUserInfo | web,ajax,CMSAuthenticate
Route::group(['middleware' => ['ajax'], 'prefix' => 'cms', 'as' => 'admin.'], function () {
    Route::get('user', ['as' => 'cms.user',   'uses' => 'Cms\AdminController@getUserInfo']);
});
