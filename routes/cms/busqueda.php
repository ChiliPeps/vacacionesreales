<?php

// Ajax Routes
// admin/busqueda/get | admin.busqueda.get | App\Http\Controllers\Cms\BusquedaController@getBusqueda | web,ajax,CMSAuthenticate
Route::group(['middleware' => ['ajax'], 'prefix' => 'busqueda', 'as' => 'admin.'], function () {
    Route::get('get', ['as' => 'busqueda.get', 'uses' => 'Cms\BusquedaController@getBusqueda']);
});
