<?php

// Ajax Routes
// admin/cms/contacto | admin.cms.contacto | App\Http\Controllers\Cms\ConfigurationController@setContacto | web,ajax,CMSAuthenticate
Route::group(['middleware' => ['ajax'], 'prefix' => 'cms', 'as' => 'admin.'], function () {
    Route::get('get',        ['as' => 'cms.get',       'uses' => 'Cms\ConfigurationController@getConfiguration']);
    Route::post('contacto',  ['as' => 'cms.contacto',  'uses' => 'Cms\ConfigurationController@setContacto']);
    Route::post('colores',   ['as' => 'cms.colores',   'uses' => 'Cms\ConfigurationController@setColores']);
    Route::post('loginback', ['as' => 'cms.loginback', 'uses' => 'Cms\ConfigurationController@setLoginback']);
    Route::post('efectos',   ['as' => 'cms.efectos',   'uses' => 'Cms\ConfigurationController@setEfectos']);
    Route::post('siteup',    ['as' => 'cms.siteup',    'uses' => 'Cms\ConfigurationController@setSiteUp']);
    Route::post('buscador',  ['as' => 'cms.buscador',  'uses' => 'Cms\ConfigurationController@setBuscadorHeader']);
    Route::post('shortcuts', ['as' => 'cms.shortcuts', 'uses' => 'Cms\ConfigurationController@setShortcuts']);
    Route::post('logo',      ['as' => 'cms.logo',      'uses' => 'Cms\ConfigurationController@setLogoCMS']);
});
