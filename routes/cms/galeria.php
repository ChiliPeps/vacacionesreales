<?php

// Ajax Routes
// admin/galeria/get | admin.galeria.get | App\Http\Controllers\Cms\GaleriaController@getGaleria | web,ajax,CMSAuthenticate
Route::group(['middleware' => ['ajax'], 'prefix' => 'galeria', 'as' => 'admin.'], function () {
    Route::get('get',       ['as' => 'galeria.get',      'uses' => 'Cms\GaleriaController@getGaleria']);
    Route::post('upload',   ['as' => 'galeria.upload',   'uses' => 'Cms\GaleriaController@uploadImagenes']);
    Route::delete('delete', ['as' => 'galeria.delete',   'uses' => 'Cms\GaleriaController@deleteImagen']);
});
