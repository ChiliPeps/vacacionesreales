<?php

// Ajax Routes
// admin/user/info | admin.user.getInfo | App\Http\Controllers\Cms\UsersController@getUserInfo | web,ajax,CMSAuthenticate
Route::group(['middleware' => ['ajax'], 'prefix' => 'users', 'as' => 'admin.'], function () {
    Route::get('get',       ['as' => 'users.get',      'uses' => 'Cms\UsersController@getUsers']);
    Route::get('get/{id}',  ['as' => 'users.get.user', 'uses' => 'Cms\UsersController@getUser']);
    Route::post('create',   ['as' => 'users.create',   'uses' => 'Cms\UsersController@createUser']);
    Route::post('update',   ['as' => 'users.update',   'uses' => 'Cms\UsersController@updateUser']);
    Route::delete('delete', ['as' => 'users.delete',   'uses' => 'Cms\UsersController@deleteUser']);
    Route::post('password', ['as' => 'users.password', 'uses' => 'Cms\UsersController@updatePassword']);
    Route::post('avatar',   ['as' => 'users.avatar',   'uses' => 'Cms\UsersController@updateAvatar']);
    Route::post('block',    ['as' => 'users.block',    'uses' => 'Cms\UsersController@toggleBlock']);
    Route::post('avatar/default', ['as' => 'users.avatar.default', 'uses' => 'Cms\UsersController@defaultAvatar']);
    Route::get('types',     ['as' => 'users.types',    'uses' => 'Cms\UsersController@getUsersTypes']);
});
