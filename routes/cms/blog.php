<?php

// Ajax Routes
// admin/blog/get | admin.blog.get | App\Http\Controllers\Cms\BlogController@getUserInfo | web,ajax,CMSAuthenticate
Route::group(['middleware' => ['ajax'], 'prefix' => 'blog', 'as' => 'admin.'], function () {
    Route::get('get',       ['as' => 'blog.get',      'uses' => 'Cms\BlogController@getPosts']);
    Route::get('get/{id}',  ['as' => 'blog.get.post', 'uses' => 'Cms\BlogController@getPost']);
    Route::post('create',   ['as' => 'blog.create',   'uses' => 'Cms\BlogController@createPost']);
    Route::post('update',   ['as' => 'blog.update',   'uses' => 'Cms\BlogController@updatePost']);
    Route::delete('delete', ['as' => 'blog.delete',   'uses' => 'Cms\BlogController@deletePost']);
    Route::post('imagen',   ['as' => 'blog.imagen',   'uses' => 'Cms\BlogController@uploadImage']);
    Route::post('publish',  ['as' => 'blog.publish',  'uses' => 'Cms\BlogController@togglePublish']);
    Route::post('tinyimage', ['as' => 'blog.tinyimage', 'uses' => 'Cms\BlogController@uploadTinyImage']);
    Route::post('categoria', ['as' => 'blog.categoria', 'uses' => 'Cms\BlogController@createCategoria']);
});
