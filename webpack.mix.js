const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// Users APP & CSS
mix.js('resources/js/users/app.js', 'public/js/users.js')
.sass('resources/sass/app.scss', 'public/css/users.css');

// =============
// CMS APP & CSS
mix.js('resources/js/cms/main.js', 'public/js/cms.js')
.sass('resources/sass/cms.scss', 'public/css/cms.css')
.extract([
    'admin-lte',
    'bootstrap',
    'axios',
    'jquery',
    'vue',
    'vuex',
    'vue-router',
    'vue-upload-component',
    'icheck-bootstrap',
    'vue-multiselect',
    'vue2-datepicker',
    'vue-tabs-component',
    'vue-easy-lightbox',
    'vue-load-image'
], 'public/js/cms_vendor.js')
.autoload({
    jquery: ['$', 'jQuery', 'jquery', 'window.jQuery']
});

mix.setPublicPath('/');
mix.setResourceRoot('../');
//mix.browserSync('http://localhost/komvac_template_v6/public');
  